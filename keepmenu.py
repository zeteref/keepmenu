#!/usr/bin/env python3
# encoding:utf8
"""Read and copy Keepass database entries using dmenu or rofi

"""
import configparser
import argparse
import logging

from functools import partial
from contextlib import closing
from enum import Enum
import errno
import re
import itertools
import locale
from multiprocessing import Event, Process, Queue
from multiprocessing.managers import BaseManager
import os
from os.path import exists, expanduser
import random
import shlex
import socket
import string
import sys
from subprocess import call, Popen, PIPE
import tempfile
from threading import Timer
import time
import re
import webbrowser
import construct
from pynput import keyboard
from pykeepass import PyKeePass
from systemd import journal


LOG = logging.getLogger(__name__)
LOG.addHandler(journal.JournaldLogHandler(identifier='keepmenu'))
logging.basicConfig(level=logging.INFO)

try:
    # secrets only available python 3.6+
    from secrets import choice
except ImportError:
    def choice(seq):
        """Provide `choice` function call for pw generation

        """
        return random.SystemRandom().choice(seq)


AUTH_FILE = expanduser("~/.cache/.keepmenu-auth")
CONF_FILE = expanduser("~/.config/keepmenu/config.ini")

class MenuOption(Enum):
    ViewEntry = 0
    Edit = 1
    Add = 2
    ManageGroups = 3
    ReloadDB = 4
    KillDaemon = 5
    TypePassword = 6
    TypeEntry = 7
    TypeUsername = 8
    TypePrevUsername = 9
    TypePrevPassword = 10
    ShowPrevEntry = 11
    TypePrev = 12

    def description(self):
        return {
            self.TypePassword:'Type password',
            self.ViewEntry:'View Individual entry',
            self.Edit:'Edit entries',
            self.Add:'Add entry',
            self.ManageGroups:'Manage groups',
            self.TypeUsername:'Type username',
            self.ReloadDB:'Reload database',
            self.KillDaemon:'Kill Keepmenu daemon',
            self.TypeEntry:'Select entry to autotype',
        }.get(self)

def find_free_port():
    """Find random free port to use for BaseManager server

    Returns: int Port

    """
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.bind(('127.0.0.1', 0))  # pylint:disable=no-member
        return sock.getsockname()[1]  # pylint:disable=no-member


def random_str():
    """Generate random auth string for BaseManager

    Returns: string

    """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(15))


def gen_passwd(chars, length=20):
    """Generate password (min = # of distinct character sets picked)

    Args: chars - Dict {preset_name_1: {char_set_1: string, char_set_2: string},
                        preset_name_2: ....}
          length - int (default 20)

    Returns: password - string OR False

    """
    sets = set()
    if chars:
        sets = set(j for i in chars.values() for j in i.values())
    if length < len(sets) or not chars:
        return False
    alphabet = "".join(set("".join(j for j in i.values()) for i in chars.values()))
    # Ensure minimum of one char from each character set
    password = "".join(choice(k) for k in sets)
    password += "".join(choice(alphabet) for i in range(length - len(sets)))
    tpw = list(password)
    random.shuffle(tpw)
    return "".join(tpw)


def process_config():
    """Set global variables. Read the config file. Create default config file if
    one doesn't exist.

    """
    # pragma pylint: disable=global-variable-undefined
    global CACHE_PERIOD_MIN, \
        CACHE_PERIOD_DEFAULT_MIN, \
        CONF, \
        DMENU_LEN, \
        ENV, \
        ENC, \
        SEQUENCE
    # pragma pylint: enable=global-variable-undefined
    ENV = os.environ.copy()
    ENV['LC_ALL'] = 'C'
    ENC = locale.getpreferredencoding()
    CACHE_PERIOD_DEFAULT_MIN = 3600 * 20
    SEQUENCE = "{USERNAME}{TAB}{PASSWORD}{ENTER}"
    CONF = configparser.ConfigParser()
    if not exists(CONF_FILE):
        try:
            os.mkdir(os.path.dirname(CONF_FILE))
        except OSError:
            pass
        with open(CONF_FILE, 'w') as conf_file:
            CONF.add_section('dmenu')
            CONF.set('dmenu', 'dmenu_command', 'dmenu')
            CONF.add_section('dmenu_passphrase')
            CONF.set('dmenu_passphrase', 'nf', '#222222')
            CONF.set('dmenu_passphrase', 'nb', '#222222')
            CONF.set('dmenu_passphrase', 'rofi_obscure', 'True')
            CONF.add_section('database')
            CONF.set('database', 'database_1', '')
            CONF.set('database', 'keyfile_1', '')
            CONF.set('database', 'pw_cache_period_min', str(CACHE_PERIOD_DEFAULT_MIN))
            CONF.set('database', 'autotype_default', SEQUENCE)
            CONF.write(conf_file)
    try:
        CONF.read(CONF_FILE)
    except configparser.ParsingError as err:
        dmenu_err("Config file error: {}".format(err))
        sys.exit()
    if CONF.has_option("database", "pw_cache_period_min"):
        CACHE_PERIOD_MIN = int(CONF.get("database", "pw_cache_period_min"))
    else:
        CACHE_PERIOD_MIN = CACHE_PERIOD_DEFAULT_MIN
    if CONF.has_option("dmenu", "l"):
        DMENU_LEN = int(CONF.get("dmenu", "l"))
    else:
        DMENU_LEN = 24
    if CONF.has_option('database', 'autotype_default'):
        SEQUENCE = CONF.get("database", "autotype_default")
    if CONF.has_option("database", "type_library"):
        if CONF.get("database", "type_library") == "xdotool":
            try:
                call(['xdotool', 'version'])
            except OSError:
                dmenu_err("Xdotool not installed.\n"
                          "Please install or remove that option from config.ini")
                sys.exit()
        elif CONF.get("database", "type_library") == "ydotool":
            try:
                call(['ydotool'])
            except OSError:
                dmenu_err("Ydotool not installed.\n"
                          "Please install or remove that option from config.ini")
                sys.exit()


def get_auth():
    """Generate and save port and authkey to ~/.cache/.keepmenu-auth

    Returns: int port, bytestring authkey

    """
    auth = configparser.ConfigParser()
    if not exists(AUTH_FILE):
        fd = os.open(AUTH_FILE, os.O_WRONLY | os.O_CREAT, 0o600)
        with open(fd, 'w') as a_file:
            auth.set('DEFAULT', 'port', str(find_free_port()))
            auth.set('DEFAULT', 'authkey', random_str())
            auth.write(a_file)
    try:
        auth.read(AUTH_FILE)
        port = auth.get('DEFAULT', 'port')
        authkey = auth.get('DEFAULT', 'authkey').encode()
    except (configparser.NoOptionError, configparser.MissingSectionHeaderError):
        os.remove(AUTH_FILE)
        print("Cache file was corrupted. Stopping all instances. Please try again")
        call(["pkill", "keepmenu"])  # Kill all prior instances as well
        return None, None
    return int(port), authkey


def dmenu_cmd(num_lines, prompt):
    """Parse config.ini for dmenu options

    Args: args - num_lines: number of lines to display
                 prompt: prompt to show
    Returns: command invocation (as a list of strings) for
                dmenu -l <num_lines> -p <prompt> -i ...

    """
    args_dict = {"dmenu_command": "dmenu"}
    if CONF.has_section('dmenu'):
        args = CONF.items('dmenu')
        args_dict.update(dict(args))
    command = shlex.split(args_dict["dmenu_command"])
    dmenu_command = command[0]
    dmenu_args = command[1:]
    del args_dict["dmenu_command"]
    lines = "-i -dmenu -multi-select -lines" if "rofi" in dmenu_command else "-i -l"
    if "l" in args_dict:
        lines = "{} {}".format(lines, min(num_lines, int(args_dict['l'])))
        del args_dict['l']
    else:
        lines = "{} {}".format(lines, num_lines)
    if "pinentry" in args_dict:
        del args_dict["pinentry"]
    if prompt == "Passphrase":
        if CONF.has_section('dmenu_passphrase'):
            args = CONF.items('dmenu_passphrase')
            args_dict.update(args)
        rofi_obscure = True
        if CONF.has_option('dmenu_passphrase', 'rofi_obscure'):
            rofi_obscure = CONF.getboolean('dmenu_passphrase', 'rofi_obscure')
            del args_dict["rofi_obscure"]
        if rofi_obscure is True and "rofi" in dmenu_command:
            dmenu_args.extend(["-password"])
    extras = (["-" + str(k), str(v)] for (k, v) in args_dict.items())
    dmenu = [dmenu_command, "-p", str(prompt)]
    dmenu.extend(dmenu_args)
    dmenu += list(itertools.chain.from_iterable(extras))
    dmenu[1:1] = lines.split()
    dmenu = list(filter(None, dmenu))  # Remove empty list elements
    return dmenu


def dmenu_select(num_lines, prompt="Entries", inp=""):
    """Call dmenu and return the selected entry

    Args: num_lines - number of lines to display
          prompt - prompt to show
          inp - bytes string to pass to dmenu via STDIN

    Returns: sel - string

    """
    cmd = dmenu_cmd(num_lines, prompt)
    sel, err = Popen(cmd,
                     stdin=PIPE,
                     stdout=PIPE,
                     stderr=PIPE,
                     env=ENV).communicate(input=inp)
    if err:
        cmd = [cmd[0]] + ["-dmenu"] if "rofi" in cmd[0] else [""]
        Popen(cmd[0], stdin=PIPE, stdout=PIPE, env=ENV).communicate(input=err)
        sys.exit()
    if sel is not None:
        sel = sel.decode(ENC).rstrip('\n')
    return sel


def dmenu_err(prompt):
    """Pops up a dmenu prompt with an error message

    """
    return dmenu_select(1, prompt)


def get_password_chars():
    """Get characters to use for password generation from defaults, config file
    and user input.

    Returns: Dict {preset_name_1: {char_set_1: string, char_set_2: string},
                   preset_name_2: ....}
    """
    chars = {"upper": string.ascii_uppercase,
             "lower": string.ascii_lowercase,
             "digits": string.digits,
             "punctuation": string.punctuation}
    presets = {}
    presets["Letters+Digits+Punctuation"] = chars
    presets["Letters+Digits"] = {k: chars[k] for k in ("upper", "lower", "digits")}
    presets["Letters"] = {k: chars[k] for k in ("upper", "lower")}
    presets["Digits"] = {k: chars[k] for k in ("digits",)}
    if CONF.has_section('password_chars'):
        pw_chars = dict(CONF.items('password_chars'))
        chars.update(pw_chars)
        for key, val in pw_chars.items():
            presets[key.title()] = {k: chars[k] for k in (key,)}
    if CONF.has_section('password_char_presets'):
        if CONF.options('password_char_presets'):
            presets = {}
        for name, val in CONF.items('password_char_presets'):
            try:
                presets[name.title()] = {k: chars[k] for k in shlex.split(val)}
            except KeyError:
                print("Error: Unknown value in preset {}. Ignoring.".format(name))
                continue
    input_b = "\n".join(presets).encode(ENC)
    char_sel = dmenu_select(len(presets),
                            "Pick character set(s) to use", inp=input_b)
    # This dictionary return also handles Rofi multiple select
    return {k: presets[k] for k in char_sel.split('\n')} if char_sel else False


def get_database():
    """Read databases from config or ask for user input.

    Returns: (database name, keyfile, passphrase)
             Returns (None, None, None) on error selecting database

    """
    args = CONF.items('database')
    args_dict = dict(args)
    dbases = [i for i in args_dict if i.startswith('database')]
    dbs = []
    for dbase in dbases:
        dbn = expanduser(args_dict[dbase])
        idx = dbase.rsplit('_', 1)[-1]
        try:
            keyfile = expanduser(args_dict['keyfile_{}'.format(idx)])
        except KeyError:
            keyfile = ''
        try:
            passw = args_dict['password_{}'.format(idx)]
        except KeyError:
            passw = ''
        try:
            cmd = args_dict['password_cmd_{}'.format(idx)]
            res = Popen(shlex.split(cmd), stdout=PIPE, stderr=PIPE).communicate()
            if res[1]:
                dmenu_err("Password command error: {}".format(res[1]))
                sys.exit()
            else:
                passw = res[0].decode().rstrip('\n') if res[0] else passw
        except KeyError:
            pass
        if dbn:
            dbs.append((dbn, keyfile, passw))
    if not dbs:
        res = get_initial_db()
        if res is True:
            dbs = [get_database()]
        else:
            return (None, None, None)
    if len(dbs) > 1:
        inp_bytes = "\n".join(i[0] for i in dbs).encode(ENC)
        sel = dmenu_select(len(dbs), "Select Database", inp=inp_bytes)
        dbs = [i for i in dbs if i[0] == sel]
        if not sel or not dbs:
            return (None, None, None)
    if not dbs[0][-1]:
        db_l = list(dbs[0])
        db_l[-1] = get_passphrase()
        dbs[0] = db_l
    return dbs[0]


def get_initial_db():
    """Ask for initial database name and keyfile if not entered in config file

    """
    db_name = dmenu_select(0, "Enter path to existing "
                              "Keepass database. ~/ for $HOME is ok")
    if not db_name:
        dmenu_err("No database entered. Try again.")
        return False
    keyfile_name = dmenu_select(0, "Enter path to keyfile. ~/ for $HOME is ok")
    with open(CONF_FILE, 'w') as conf_file:
        CONF.set('database', 'database_1', db_name)
        if keyfile_name:
            CONF.set('database', 'keyfile_1', keyfile_name)
        CONF.write(conf_file)
    return True


def get_entries(dbo):
    """Open keepass database and return the PyKeePass object

        Args: dbo: tuple (db path, keyfile path, password)
        Returns: PyKeePass object

    """
    dbf, keyfile, password = dbo
    if dbf is None:
        return None
    try:
        kpo = PyKeePass(dbf, password, keyfile=keyfile)
    except (FileNotFoundError, construct.core.ChecksumError) as err:
        if str(err.args[0]).startswith("wrong checksum"):
            dmenu_err("Invalid Password or keyfile")
            return None
        try:
            if err.errno == errno.ENOENT:
                if not os.path.isfile(dbf):
                    dmenu_err("Database does not exist. Edit ~/.config/keepmenu/config.ini")
                elif not os.path.isfile(keyfile):
                    dmenu_err("Keyfile does not exist. Edit ~/.config/keepmenu/config.ini")
        except AttributeError:
            pass
        return None
    except Exception as err:
        dmenu_err("Error: {}".format(err))
        return None
    return kpo


def get_passphrase():
    """Get a database password from dmenu or pinentry

    Returns: string

    """
    pinentry = None
    if CONF.has_option("dmenu", "pinentry"):
        pinentry = CONF.get("dmenu", "pinentry")
    if pinentry:
        password = ""
        out = Popen(pinentry,
                    stdout=PIPE,
                    stdin=PIPE).communicate(
                        input=b'setdesc Enter database password\ngetpin\n')[0]
        if out:
            res = out.decode(ENC).split("\n")[2]
            if res.startswith("D "):
                password = res.split("D ")[1]
    else:
        password = dmenu_select(0, "Passphrase")
        if not password:
            sys.exit()
    return password


def tokenize_autotype(autotype):
    """Process the autotype sequence

    Args: autotype - string
    Returns: tokens - generator ((token, if_special_char T/F), ...)

    """
    while autotype:
        opening_idx = -1
        for char in "{+^%~@":
            idx = autotype.find(char)
            if idx != -1 and (opening_idx == -1 or idx < opening_idx):
                opening_idx = idx

        if opening_idx == -1:
            # found the end of the string without further opening braces or
            # other characters
            yield autotype, False
            return

        if opening_idx > 0:
            yield autotype[:opening_idx], False

        if autotype[opening_idx] in "+^%~@":
            yield autotype[opening_idx], True
            autotype = autotype[opening_idx + 1:]
            continue

        closing_idx = autotype.find('}')
        if closing_idx == -1:
            dmenu_err("Unable to find matching right brace (}) while" +
                      "tokenizing auto-type string: %s\n" % (autotype))
            return
        if closing_idx == opening_idx + 1 and closing_idx + 1 < len(autotype) \
                and autotype[closing_idx + 1] == '}':
            yield "{}}", True
            autotype = autotype[closing_idx + 2:]
            continue
        yield autotype[opening_idx:closing_idx + 1], True
        autotype = autotype[closing_idx + 1:]


def token_command(token):
    """When token denotes a special command, this function provides a callable
    implementing its behaviour.

    """
    cmd = None

    def _check_delay():
        match = re.match(r'{DELAY (\d+)}', token)
        if match:
            delay = match.group(1)
            nonlocal cmd
            cmd = lambda t=delay: time.sleep(int(t) / 1000)
            return True
        return False

    if _check_delay():  # {DELAY x}
        return cmd
    return None


def type_entry(entry):
    """Pick which library to use to type strings

    Defaults to pynput

    """
    sequence = SEQUENCE
    if hasattr(entry, 'autotype_enabled') and entry.autotype_enabled is False:
        dmenu_err("Autotype disabled for this entry")
        return
    if hasattr(entry, 'autotype_sequence') and \
            entry.autotype_sequence is not None and \
            entry.autotype_sequence != 'None':
        sequence = entry.autotype_sequence
    tokens = tokenize_autotype(sequence)

    library = 'pynput'
    if CONF.has_option('database', 'type_library'):
        library = CONF.get('database', 'type_library')
    if library == 'xdotool':
        type_entry_xdotool(entry, tokens)
    elif library == 'ydotool':
        type_entry_ydotool(entry, tokens)
    else:
        type_entry_pynput(entry, tokens)


PLACEHOLDER_AUTOTYPE_TOKENS = {
    "{TITLE}"   : lambda e: e.title,
    "{USERNAME}": lambda e: e.username,
    "{URL}"     : lambda e: e.url,
    "{PASSWORD}": lambda e: e.password,
    "{NOTES}"   : lambda e: e.notes,
}

STRING_AUTOTYPE_TOKENS = {
    "{PLUS}"      : '+',
    "{PERCENT}"   : '%',
    "{CARET}"     : '^',
    "{TILDE}"     : '~',
    "{LEFTPAREN}" : '(',
    "{RIGHTPAREN}": ')',
    "{LEFTBRACE}" : '{',
    "{RIGHTBRACE}": '}',
    "{AT}"        : '@',
    "{+}"         : '+',
    "{%}"         : '%',
    "{^}"         : '^',
    "{~}"         : '~',
    "{(}"         : '(',
    "{)}"         : ')',
    "{[}"         : '[',
    "{]}"         : ']',
    "{{}"         : '{',
    "{}}"         : '}',
}

PYNPUT_AUTOTYPE_TOKENS = {
    "{TAB}"       : keyboard.Key.tab,
    "{ENTER}"     : keyboard.Key.enter,
    "~"           : keyboard.Key.enter,
    "{UP}"        : keyboard.Key.up,
    "{DOWN}"      : keyboard.Key.down,
    "{LEFT}"      : keyboard.Key.left,
    "{RIGHT}"     : keyboard.Key.right,
    "{INSERT}"    : keyboard.Key.insert,
    "{INS}"       : keyboard.Key.insert,
    "{DELETE}"    : keyboard.Key.delete,
    "{DEL}"       : keyboard.Key.delete,
    "{HOME}"      : keyboard.Key.home,
    "{END}"       : keyboard.Key.end,
    "{PGUP}"      : keyboard.Key.page_up,
    "{PGDN}"      : keyboard.Key.page_down,
    "{SPACE}"     : keyboard.Key.space,
    "{BACKSPACE}" : keyboard.Key.backspace,
    "{BS}"        : keyboard.Key.backspace,
    "{BKSP}"      : keyboard.Key.backspace,
    "{BREAK}"     : keyboard.Key.pause,
    "{CAPSLOCK}"  : keyboard.Key.caps_lock,
    "{ESC}"       : keyboard.Key.esc,
    "{WIN}"       : keyboard.Key.cmd,
    "{LWIN}"      : keyboard.Key.cmd_l,
    "{RWIN}"      : keyboard.Key.cmd_r,
    # "{APPS}"    : keyboard.Key.
    # "{HELP}"    : keyboard.Key.
    "{NUMLOCK}"   : keyboard.Key.num_lock,
    "{PRTSC}"     : keyboard.Key.print_screen,
    "{SCROLLLOCK}": keyboard.Key.scroll_lock,
    "{F1}"        : keyboard.Key.f1,
    "{F2}"        : keyboard.Key.f2,
    "{F3}"        : keyboard.Key.f3,
    "{F4}"        : keyboard.Key.f4,
    "{F5}"        : keyboard.Key.f5,
    "{F6}"        : keyboard.Key.f6,
    "{F7}"        : keyboard.Key.f7,
    "{F8}"        : keyboard.Key.f8,
    "{F9}"        : keyboard.Key.f9,
    "{F10}"       : keyboard.Key.f10,
    "{F11}"       : keyboard.Key.f11,
    "{F12}"       : keyboard.Key.f12,
    "{F13}"       : keyboard.Key.f13,
    "{F14}"       : keyboard.Key.f14,
    "{F15}"       : keyboard.Key.f15,
    "{F16}"       : keyboard.Key.f16,
    # "{ADD}"       : keyboard.Key.
    # "{SUBTRACT}"  : keyboard.Key.
    # "{MULTIPLY}"  : keyboard.Key.
    # "{DIVIDE}"    : keyboard.Key.
    # "{NUMPAD0}"   : keyboard.Key.
    # "{NUMPAD1}"   : keyboard.Key.
    # "{NUMPAD2}"   : keyboard.Key.
    # "{NUMPAD3}"   : keyboard.Key.
    # "{NUMPAD4}"   : keyboard.Key.
    # "{NUMPAD5}"   : keyboard.Key.
    # "{NUMPAD6}"   : keyboard.Key.
    # "{NUMPAD7}"   : keyboard.Key.
    # "{NUMPAD8}"   : keyboard.Key.
    # "{NUMPAD9}"   : keyboard.Key.
    "+"           : keyboard.Key.shift,
    "^"           : keyboard.Key.ctrl,
    "%"           : keyboard.Key.alt,
    "@"           : keyboard.Key.cmd,
}


def type_entry_pynput(entry, tokens):
    """Use pynput to auto-type the selected entry

    """
    kbd = keyboard.Controller()
    enter_idx = True
    for token, special in tokens:
        if special:
            cmd = token_command(token)
            if callable(cmd):
                cmd()
            elif token in PLACEHOLDER_AUTOTYPE_TOKENS:
                to_type = PLACEHOLDER_AUTOTYPE_TOKENS[token](entry)
                if to_type:
                    try:
                        kbd.type(to_type)
                    except kbd.InvalidCharacterException:
                        dmenu_err("Unable to type string...bad character.\n"
                                  "Try setting `type_library = xdotool` in config.ini")
                        return
            elif token in STRING_AUTOTYPE_TOKENS:
                to_type = STRING_AUTOTYPE_TOKENS[token]
                try:
                    kbd.type(to_type)
                except kbd.InvalidCharacterException:
                    dmenu_err("Unable to type string...bad character.\n"
                              "Try setting `type_library = xdotool` in config.ini")
                    return
            elif token in PYNPUT_AUTOTYPE_TOKENS:
                to_tap = PYNPUT_AUTOTYPE_TOKENS[token]
                kbd.tap(to_tap)
                # Add extra {ENTER} key tap for first instance of {ENTER}. It
                # doesn't get recognized for some reason.
                if enter_idx is True and token in ("{ENTER}", "~"):
                    kbd.tap(to_tap)
                    enter_idx = False
            else:
                dmenu_err("Unsupported auto-type token (pynput): \"%s\"" % (token))
                return
        else:
            try:
                kbd.type(token)
            except kbd.InvalidCharacterException:
                dmenu_err("Unable to type string...bad character.\n"
                          "Try setting `type_library = xdotool` in config.ini")
                return


XDOTOOL_AUTOTYPE_TOKENS = {
    "{TAB}"       : ['key', 'Tab'],
    "{ENTER}"     : ['key', 'Return'],
    "~"           : ['key', 'Return'],
    "{UP}"        : ['key', 'Up'],
    "{DOWN}"      : ['key', 'Down'],
    "{LEFT}"      : ['key', 'Left'],
    "{RIGHT}"     : ['key', 'Right'],
    "{INSERT}"    : ['key', 'Insert'],
    "{INS}"       : ['key', 'Insert'],
    "{DELETE}"    : ['key', 'Delete'],
    "{DEL}"       : ['key', 'Delete'],
    "{HOME}"      : ['key', 'Home'],
    "{END}"       : ['key', 'End'],
    "{PGUP}"      : ['key', 'Page_Up'],
    "{PGDN}"      : ['key', 'Page_Down'],
    "{SPACE}"     : ['type', ' '],
    "{BACKSPACE}" : ['key', 'BackSpace'],
    "{BS}"        : ['key', 'BackSpace'],
    "{BKSP}"      : ['key', 'BackSpace'],
    "{BREAK}"     : ['key', 'Break'],
    "{CAPSLOCK}"  : ['key', 'Caps_Lock'],
    "{ESC}"       : ['key', 'Escape'],
    "{WIN}"       : ['key', 'Super'],
    "{LWIN}"      : ['key', 'Super_L'],
    "{RWIN}"      : ['key', 'Super_R'],
    # "{APPS}"      : ['key', ''],
    # "{HELP}"      : ['key', ''],
    "{NUMLOCK}"   : ['key', 'Num_Lock'],
    # "{PRTSC}"     : ['key', ''],
    "{SCROLLLOCK}": ['key', 'Scroll_Lock'],
    "{F1}"        : ['key', 'F1'],
    "{F2}"        : ['key', 'F2'],
    "{F3}"        : ['key', 'F3'],
    "{F4}"        : ['key', 'F4'],
    "{F5}"        : ['key', 'F5'],
    "{F6}"        : ['key', 'F6'],
    "{F7}"        : ['key', 'F7'],
    "{F8}"        : ['key', 'F8'],
    "{F9}"        : ['key', 'F9'],
    "{F10}"       : ['key', 'F10'],
    "{F11}"       : ['key', 'F11'],
    "{F12}"       : ['key', 'F12'],
    "{F13}"       : ['key', 'F13'],
    "{F14}"       : ['key', 'F14'],
    "{F15}"       : ['key', 'F15'],
    "{F16}"       : ['key', 'F16'],
    "{ADD}"       : ['key', 'KP_Add'],
    "{SUBTRACT}"  : ['key', 'KP_Subtract'],
    "{MULTIPLY}"  : ['key', 'KP_Multiply'],
    "{DIVIDE}"    : ['key', 'KP_Divide'],
    "{NUMPAD0}"   : ['key', 'KP_0'],
    "{NUMPAD1}"   : ['key', 'KP_1'],
    "{NUMPAD2}"   : ['key', 'KP_2'],
    "{NUMPAD3}"   : ['key', 'KP_3'],
    "{NUMPAD4}"   : ['key', 'KP_4'],
    "{NUMPAD5}"   : ['key', 'KP_5'],
    "{NUMPAD6}"   : ['key', 'KP_6'],
    "{NUMPAD7}"   : ['key', 'KP_7'],
    "{NUMPAD8}"   : ['key', 'KP_8'],
    "{NUMPAD9}"   : ['key', 'KP_9'],
    "+"           : ['key', 'Shift'],
    "^"           : ['Key', 'Ctrl'],
    "%"           : ['key', 'Alt'],
    "@"           : ['key', 'Super'],
}


def type_entry_xdotool(entry, tokens):
    """Auto-type entry entry using xdotool

    """
    enter_idx = True
    for token, special in tokens:
        if special:
            cmd = token_command(token)
            if callable(cmd):
                cmd()
            elif token in PLACEHOLDER_AUTOTYPE_TOKENS:
                to_type = PLACEHOLDER_AUTOTYPE_TOKENS[token](entry)
                if to_type:
                    call(['xdotool', 'type', to_type])
            elif token in STRING_AUTOTYPE_TOKENS:
                to_type = STRING_AUTOTYPE_TOKENS[token]
                call(['xdotool', 'type', to_type])
            elif token in XDOTOOL_AUTOTYPE_TOKENS:
                cmd = ['xdotool'] + XDOTOOL_AUTOTYPE_TOKENS[token]
                call(cmd)
                # Add extra {ENTER} key tap for first instance of {ENTER}. It
                # doesn't get recognized for some reason.
                if enter_idx is True and token in ("{ENTER}", "~"):
                    cmd = ['xdotool'] + XDOTOOL_AUTOTYPE_TOKENS[token]
                    call(cmd)
                    enter_idx = False
            else:
                dmenu_err("Unsupported auto-type token (xdotool): \"%s\"" % (token))
                return
        else:
            call(['xdotool', 'type', token])


YDOTOOL_AUTOTYPE_TOKENS = {
    "{TAB}"       : ['key', 'TAB'],
    "{ENTER}"     : ['key', 'ENTER'],
    "~"           : ['key', 'Return'],
    "{UP}"        : ['key', 'UP'],
    "{DOWN}"      : ['key', 'DOWN'],
    "{LEFT}"      : ['key', 'LEFT'],
    "{RIGHT}"     : ['key', 'RIGHT'],
    "{INSERT}"    : ['key', 'INSERT'],
    "{INS}"       : ['key', 'INSERT'],
    "{DELETE}"    : ['key', 'DELETE'],
    "{DEL}"       : ['key', 'DELETE'],
    "{HOME}"      : ['key', 'HOME'],
    "{END}"       : ['key', 'END'],
    "{PGUP}"      : ['key', 'PAGEUP'],
    "{PGDN}"      : ['key', 'PAGEDOWN'],
    "{SPACE}"     : ['type', ' '],
    "{BACKSPACE}" : ['key', 'BACKSPACE'],
    "{BS}"        : ['key', 'BACKSPACE'],
    "{BKSP}"      : ['key', 'BACKSPACE'],
    "{BREAK}"     : ['key', 'BREAK'],
    "{CAPSLOCK}"  : ['key', 'CAPSLOCK'],
    "{ESC}"       : ['key', 'ESC'],
    # "{WIN}"       : ['key', 'Super'],
    # "{LWIN}"      : ['key', 'Super_L'],
    # "{RWIN}"      : ['key', 'Super_R'],
    # "{APPS}"      : ['key', ''],
    # "{HELP}"      : ['key', ''],
    "{NUMLOCK}"   : ['key', 'NUMLOCK'],
    # "{PRTSC}"     : ['key', ''],
    "{SCROLLLOCK}": ['key', 'SCROLLLOCK'],
    "{F1}"        : ['key', 'F1'],
    "{F2}"        : ['key', 'F2'],
    "{F3}"        : ['key', 'F3'],
    "{F4}"        : ['key', 'F4'],
    "{F5}"        : ['key', 'F5'],
    "{F6}"        : ['key', 'F6'],
    "{F7}"        : ['key', 'F7'],
    "{F8}"        : ['key', 'F8'],
    "{F9}"        : ['key', 'F9'],
    "{F10}"       : ['key', 'F10'],
    "{F11}"       : ['key', 'F11'],
    "{F12}"       : ['key', 'F12'],
    "{F13}"       : ['key', 'F13'],
    "{F14}"       : ['key', 'F14'],
    "{F15}"       : ['key', 'F15'],
    "{F16}"       : ['key', 'F16'],
    "{ADD}"       : ['key', 'KPPLUS'],
    "{SUBTRACT}"  : ['key', 'KPMINUS'],
    "{MULTIPLY}"  : ['key', 'KPASTERISK'],
    "{DIVIDE}"    : ['key', 'KPSLASH'],
    "{NUMPAD0}"   : ['key', 'KP0'],
    "{NUMPAD1}"   : ['key', 'KP1'],
    "{NUMPAD2}"   : ['key', 'KP2'],
    "{NUMPAD3}"   : ['key', 'KP3'],
    "{NUMPAD4}"   : ['key', 'KP4'],
    "{NUMPAD5}"   : ['key', 'KP5'],
    "{NUMPAD6}"   : ['key', 'KP6'],
    "{NUMPAD7}"   : ['key', 'KP7'],
    "{NUMPAD8}"   : ['key', 'KP8'],
    "{NUMPAD9}"   : ['key', 'KP9'],
    "+"           : ['key', 'LEFTSHIFT'],
    "^"           : ['Key', 'LEFTCTRL'],
    "%"           : ['key', 'LEFTALT'],
    # "@"           : ['key', 'Super']
}


def type_entry_ydotool(entry, tokens):
    """Auto-type entry entry using ydotool

    """
    enter_idx = True
    for token, special in tokens:
        if special:
            cmd = token_command(token)
            if callable(cmd):
                cmd()
            elif token in PLACEHOLDER_AUTOTYPE_TOKENS:
                to_type = PLACEHOLDER_AUTOTYPE_TOKENS[token](entry)
                if to_type:
                    call(['ydotool', 'type', to_type])
            elif token in STRING_AUTOTYPE_TOKENS:
                to_type = STRING_AUTOTYPE_TOKENS[token]
                call(['ydotool', 'type', to_type])
            elif token in YDOTOOL_AUTOTYPE_TOKENS:
                cmd = ['ydotool'] + YDOTOOL_AUTOTYPE_TOKENS[token]
                call(cmd)
                # Add extra {ENTER} key tap for first instance of {ENTER}. It
                # doesn't get recognized for some reason.
                if enter_idx is True and token in ("{ENTER}", "~"):
                    cmd = ['ydotool'] + YDOTOOL_AUTOTYPE_TOKENS[token]
                    call(cmd)
                    enter_idx = False
            else:
                dmenu_err("Unsupported auto-type token (ydotool): \"%s\"" % (token))
                return
        else:
            call(['ydotool', 'type', token])


def type_text(data):
    """Type the given text data

    """
    library = 'pynput'
    if CONF.has_option('database', 'type_library'):
        library = CONF.get('database', 'type_library')
    if library == 'xdotool':
        call(['xdotool', 'type', data])
    elif library == 'ydotool':
        call(['ydotool', 'type', data])
    else:
        kbd = keyboard.Controller()
        try:
            kbd.type(data)
        except kbd.InvalidCharacterException:
            dmenu_err("Unable to type string...bad character.\n"
                      "Try setting `type_library = xdotool` in config.ini")


def view_all_entries(options, entries_descriptions, prompt='Entries'):
    """Generate numbered list of all Keepass entries and open with dmenu.

    Returns: dmenu selection

    """

    kp_entries_b = str("\n").join(entries_descriptions).encode(ENC)
    if options:
        options_b = ("\n".join(map(str, options)) + "\n").encode(ENC)
        entries_b = options_b + kp_entries_b
    else:
        entries_b = kp_entries_b
    return dmenu_select(min(DMENU_LEN, len(options) + len(entries_descriptions)), prompt, inp=entries_b)


def select_group(kpo, prompt="Groups"):
    """Select which group for an entry

    Args: kpo - Keepass object
          options - list of menu options for groups

    Returns: False for no entry
             group - string

    """
    groups = kpo.groups
    num_align = len(str(len(groups)))
    pattern = str("{:>{na}} - {}")
    input_b = str("\n").join([pattern.format(j, i.path, na=num_align)
                              for j, i in enumerate(groups)]).encode(ENC)
    sel = dmenu_select(min(DMENU_LEN, len(groups)), prompt, inp=input_b)
    if not sel:
        return False
    try:
        return groups[int(sel.split('-', 1)[0])]
    except (ValueError, TypeError):
        return False


def manage_groups(kpo):
    """Rename, create, move or delete groups

    Args: kpo - Keepass object
    Returns: Group object or False

    """
    edit = True
    options = ['Create',
               'Move',
               'Rename',
               'Delete']
    group = False
    while edit is True:
        input_b = b"\n".join(i.encode(ENC) for i in options) + b"\n\n" + \
            b"\n".join(i.path.encode(ENC) for i in kpo.groups)
        sel = dmenu_select(len(options) + len(kpo.groups) + 1, "Groups", inp=input_b)
        if not sel:
            edit = False
        elif sel == 'Create':
            group = create_group(kpo)
        elif sel == 'Move':
            group = move_group(kpo)
        elif sel == 'Rename':
            group = rename_group(kpo)
        elif sel == 'Delete':
            group = delete_group(kpo)
        else:
            edit = False
    return group


def create_group(kpo):
    """Create new group

    Args: kpo - Keepass object
    Returns: Group object or False

    """
    parentgroup = select_group(kpo, prompt="Select parent group")
    if not parentgroup:
        return False
    name = dmenu_select(1, "Group name")
    if not name:
        return False
    group = kpo.add_group(parentgroup, name)
    kpo.save()
    return group


def delete_group(kpo):
    """Delete a group

    Args: kpo - Keepass object
    Returns: Group object or False

    """
    group = select_group(kpo, prompt="Delete Group:")
    if not group:
        return False
    input_b = b"NO\nYes - confirm delete\n"
    delete = dmenu_select(2, "Confirm delete", inp=input_b)
    if delete != "Yes - confirm delete":
        return True
    kpo.delete_group(group)
    kpo.save()
    return group


def move_group(kpo):
    """Move group

    Args: kpo - Keepass object
    Returns: Group object or False

    """
    group = select_group(kpo, prompt="Select group to move")
    if not group:
        return False
    destgroup = select_group(kpo, prompt="Select destination group")
    if not destgroup:
        return False
    group = kpo.move_group(group, destgroup)
    kpo.save()
    return group


def rename_group(kpo):
    """Rename group

    Args: kpo - Keepass object
    Returns: Group object or False

    """
    group = select_group(kpo, prompt="Select group to rename")
    if not group:
        return False
    name = dmenu_select(1, "New group name", inp=group.name.encode(ENC))
    if not name:
        return False
    group.name = name
    kpo.save()
    return group


def add_entry(kpo):
    """Add Keepass entry

    Args: kpo - Keepass object
    Returns: False if not added
             Keepass Entry object on success

    """
    group = select_group(kpo)
    if group is False:
        return False
    entry = kpo.add_entry(destination_group=group, title="", username="", password="")
    edit = True
    while edit is True:
        edit = edit_entry(kpo, entry)
    return entry


def delete_entry(kpo, kp_entry):
    """Delete an entry

    Args: kpo - Keepass object
          kp_entry - keepass entry
    Returns: True if no delete
             False if delete

    """
    input_b = b"NO\nYes - confirm delete\n"
    delete = dmenu_select(2, "Confirm delete", inp=input_b)
    if delete != "Yes - confirm delete":
        return True
    kpo.delete_entry(kp_entry)
    kpo.save()
    return False


def view_entry(kp_entry):
    """Show title, username, password, url and notes for an entry.

    Returns: dmenu selection

    """
    fields = ["/".join(kp_entry.path) or "Title: None",
              kp_entry.username or "Username: None",
              '**********' if kp_entry.password else "Password: None",
              kp_entry.url or "URL: None",
              "Notes: <Enter to view>" if kp_entry.notes else "Notes: None"]

    def show_prop(key):
        val = kp_entry.custom_properties[key]
        return '*********' if key.startswith('#') else val

    fields += [f'@({key}): {show_prop(key)}' for key in kp_entry.custom_properties]

    kp_entries_b = "\n".join(fields).encode(ENC)
    sel = dmenu_select(len(fields), inp=kp_entries_b)
    if sel == "Notes: <Enter to view>":
        sel = view_notes(kp_entry.notes)
    elif sel == "Notes: None":
        sel = ""
    elif sel == '**********':
        sel = kp_entry.password
    elif sel == fields[3]:
        if sel != "URL: None":
            webbrowser.open(sel)
        sel = ""
    elif re.search('^@', str(sel)):
        return kp_entry.custom_properties[re.search(r'@\((.+)(?=\):)', str(sel)).group(1)]
    return sel


def edit_entry(kpo, kp_entry):  # pylint: disable=too-many-return-statements, too-many-branches
    """Edit title, username, password, url and autotype sequence for an entry.

    Args: kpo - Keepass object
          kp_entry - selected Entry object

    Returns: True to continue editing
             False if done

    """
    path = "/".join(kp.entry.path)
    fields = [str("Title: {}").format(kp_entry.title),
              str("Path: {}").format(path),
              str("Username: {}").format(kp_entry.username),
              str("Password: **********") if kp_entry.password else "Password: None",
              str("Url: {}").format(kp_entry.url),
              "Notes: <Enter to Edit>" if kp_entry.notes else "Notes: None",
              "Delete Entry: "]
    if hasattr(kp_entry, 'autotype_sequence') and hasattr(kp_entry, 'autotype_enabled'):
        fields[5:5] = [str("Autotype Sequence: {}").format(kp_entry.autotype_sequence),
                       str("Autotype Enabled: {}").format(kp_entry.autotype_enabled)]
    input_b = "\n".join(fields).encode(ENC)
    sel = dmenu_select(len(fields), inp=input_b)
    try:
        field, sel = sel.split(": ", 1)
    except (ValueError, TypeError):
        return False
    field = field.lower().replace(" ", "_")
    if field == 'password':
        sel = kp_entry.password
    edit_b = sel.encode(ENC) + b"\n" if sel is not None else b"\n"
    if field == 'delete_entry':
        return delete_entry(kpo, kp_entry)
    if field == 'path':
        group = select_group(kpo)
        if not group:
            return True
        kpo.move_entry(kp_entry, group)
        return True
    pw_choice = ""
    if field == 'password':
        inputs_b = [
            b"Generate password",
            b"Manually enter password",
        ]
        if kp_entry.password:
            inputs_b.append(b"Type existing password")
        pw_choice = dmenu_select(len(inputs_b), "Password", inp=b"\n".join(inputs_b))
        if pw_choice == "Manually enter password":
            pass
        elif pw_choice == "Type existing password":
            type_text(kp_entry.password)
            return False
        elif not pw_choice:
            return True
        else:
            pw_choice = ''
            input_b = b"20\n"
            length = dmenu_select(1, "Password Length?", inp=input_b)
            if not length:
                return True
            try:
                length = int(length)
            except ValueError:
                length = 20
            chars = get_password_chars()
            if chars is False:
                return True
            sel = gen_passwd(chars, length)
            if sel is False:
                dmenu_err("Number of char groups desired is more than requested pw length")
                return True

    if field == 'autotype_enabled':
        input_b = b"True\nFalse\n"
        at_enab = dmenu_select(2, "Autotype Enabled? True/False", inp=input_b)
        if not at_enab:
            return True
        sel = not at_enab == 'False'
    if (field not in ('password', 'notes', 'path', 'autotype_enabled')) or pw_choice:
        sel = dmenu_select(1, "{}".format(field.capitalize()), inp=edit_b)
        if not sel:
            return True
        if pw_choice:
            sel_check = dmenu_select(1, "{}".format(field.capitalize()), inp=edit_b)
            if not sel_check or sel_check != sel:
                dmenu_err("Passwords do not match. No changes made.")
                return True
    elif field == 'notes':
        sel = edit_notes(kp_entry.notes)
    setattr(kp_entry, field, sel)
    return True


def edit_notes(note):
    """Use $EDITOR (or 'vim' if not set) to edit the notes entry

    In configuration file:
        Set 'gui_editor' for things like emacs, gvim, leafpad
        Set 'editor' for vim, emacs -nw, nano unless $EDITOR is defined
        Set 'terminal' if using a non-gui editor

    Args: note - string
    Returns: note - string

    """
    if CONF.has_option("database", "gui_editor"):
        editor = CONF.get("database", "gui_editor")
        editor = shlex.split(editor)
    else:
        if CONF.has_option("database", "editor"):
            editor = CONF.get("database", "editor")
        else:
            editor = os.environ.get('EDITOR', 'vim')
        if CONF.has_option("database", "terminal"):
            terminal = CONF.get("database", "terminal")
        else:
            terminal = "xterm"
        terminal = shlex.split(terminal)
        editor = shlex.split(editor)
        editor = terminal + ["-e"] + editor
    note = b'' if note is None else note.encode(ENC)
    with tempfile.NamedTemporaryFile(suffix=".tmp") as fname:
        fname.write(note)
        fname.flush()
        editor.append(fname.name)
        try:
            call(editor)
        except FileNotFoundError:
            dmenu_err("Terminal not found. Please update config.ini.")
            note = '' if not note else note.decode(ENC)
            return note
        fname.seek(0)
        note = fname.read()
    note = '' if not note else note.decode(ENC)
    return note


def view_notes(notes):
    """View the 'Notes' field line-by-line within dmenu.

    Returns: text of the selected line for typing

    """
    notes_l = notes.split('\n')
    notes_b = "\n".join(notes_l).encode(ENC)
    sel = dmenu_select(min(DMENU_LEN, len(notes_l)), inp=notes_b)
    return sel


# extract _entry_description and _description_idx functions here to be able to
# easily change format of displayed entry

def _entry_description(idx, idx_align, e):
    "return text describing entry (used to select entry from menu)"
    path = "/".join(e.path)
    return f'{idx:>{idx_align}} - {path} - {e.username} - {e.url}'

def _description_idx(description):
    "extract entry idx from text used to select it"
    return int(description.split('-', 1)[0])


def show_tray():
    import base64
    import pystray

    from io import BytesIO
    from PIL import Image

    encoded_img = """
iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURB
VHic7d13mBXl2cfx71Z6WarSVaSIooKIXVFsYFfErrFr1Bg1RqNGE02MGnvvr93YsMdeUVSaIAKi
CNL7LiywbH//GDWKsOzOmXPO7s73c13nQmCfe26S3TO/88zM84AkSZIkSZIkSZIkSZIkSZIkSZIk
SZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSXVVRrobkCRVW1ug
K9ANaA+0XuvV6sdfM4CmQM6P41ryv/f7ImDNj/9dCJT9+Pula72W/PjrHOAHYCZQnKR/l9LAACBJ
tUsLYEtgqx9/7faLV5N0NfWj+QRBYCbwHfAVMPHH/y5PW1cKxQAgSenTHtgR2J5fn/DrmjXA1wSB
YALwGTAWKE1nU6qaAUCSUiMT2ALYGdjpx1f3tHaUXEXAGOAT4FNgFMFlBdUSBgBJSp42wCBgMHAA
0CG97aTdZOAV4B3gI6Akve3EmwFAkqKTQTCdfwCwH9CP4JO/fquAIAi8QRAKFqW3HUmSaq4PcBXw
LVDpq8avcmAk8AeC+yIkSaq1tgb+BXxP+k+g9elVSjArcDLQvNr/b0iSlETNgBOAt0n/iTIOryLg
GYJ7KLxkLUlKuf7AvQQL56T7pBjX11TgzwSLIUmSlDS5BJ/2x5P+k5+v/73WAA8RrJkgSVJkmhPc
jDaL9J/sfFX9GgkciJcHJEkJ6ALcAqwg/Sc2XzV7fQkcB2T/5v9VrZepSVLcdQAuBU4DGqS5l8hk
Z+fQpEljABo2bERug+CfVli4gsqKCioqKiksXJHOFpNhGvB34CmgIs291HoGAElx1Z7gprIzgUZp
7mWDMjIyaNeuPR07d6ZTp8507NSJjTbuQF5eHi3z8shr1YqWLYP/bty4cbXrVlZWUpCfT35+PssL
gl8L8vNZunQJ8+bOYc7s2cFrzmxWr1qVxH9hpCYTrMvwPAaB9TIASIqbFgSf+M8h/bvr/UZmZiad
u3Sl9xZ96NGrFz179aJHz9506tyZ3NzctPZWkJ/PjO+n883UKcFrSvBrQUFBWvuqwkSC/69fT3cj
tZEBQFJcZAGnAFcD7dLcy8/atG3Ltv23o/92A+i/3QB699mSRo1q/YTEryyYP58JX45j7OjRjBs7
hkkTJ1BaWqs2AnwTuIBgZkA/MgBIioNBwM0Eq/elVes2bdh1tz3YZffd6b/dALp07ZbuliJXXFzM
VxO+5IvPP+PjD95n7NgxlJeVpbutMuAegksDS9PbSu1gAJBUn3UlOPEfmq4GsrKy2KZff3bfYxC7
DdqTPltuRWZmvPYHKixcwScff8RH77/Phx+8x4L589PZzjLgSuBugj0IYssAIKk+ygROBf5NsHxv
ag+emcm2/bdjyAEHMvSgg2nbttZccagVvp32Da+/+govvfA8P8ycka42xhN8j4xLVwPpZgCQVN9s
BdwPDEzlQTMyMhiw/UCGHnQI+w0ZSpu2rli7IZWVlXw5biyvvfIyr7/yMgsXLkh1C6XAdcA1QHGq
D55uBgBJ9UVD4DKCR/tyUnXQNm3bctiwIxl+9LF022TTVB223ikvK+P9997lP08+zofvv0d5eUpn
56cCpwMfp/Kg6WYAkFQfbAU8SYrWh8/MzGSXXXdn+LHHMniffcnOTlneiIUF8+fz7NNP8ux/nmLu
nDmpOmwFcCNwOVCSqoOmkwFAUl2WQbCC381A9Ve/CSk3N5ehBx7Mmb8/l+49eiT7cLFXUVHB++++
wz133s64MaNTddhJwLEEawjUawYASXVVe+BBYGiyD9SqdWuGDT+ak049jXbt2if7cFqHSRMn8n8P
3s/LL76QissDa4BLgNsI9hqolwwAkuqiA4CHgTbJPEj79htx5jnnMfyYY2nQoN5sE1Cnzfh+Onfc
cjMvv/gCFRVJX+X3FeAkgkcH6x0DgKS6JAO4GPgnwaN+SZHXqhWnnXk2J558Kg0bNkzWYZSA7779
lnvuuC0VMwKzgSOAL5J5kHQwAEiqK/KAJ4D9k3WAFi1acMbvz+X4k06u0YY6Sp+pUyZz8w3X8c5b
bybzMEXAWcAjyTxIqhkAJNUFWwMvAEl5zi4rO5thw4/mgj/9mdZtknpVQUny+ahPueaqvzL560nJ
PMx9wLnUk6cEDACSarsjCa73J+Uj+a6778FlV/6NzXv0TEZ5pVB5eTnPPv0kN91wHUuXLEnWYUYB
hwCLknWAVDEASKrN/gxcSxLeqzp17syVV/+TPQfvHXVppVlh4QpuvfHfPPrwg8m6P+B7gqdPpiaj
eKoYACTVRlkEj2CdHXXhzMxMjjz6WP5yxZU0ado06vKqRSZ/PYm//OlCvpo4IRnl84HDgA+SUTwV
stLdgCStpSnwPMFiLJHq2as39z70CMcefyK5ublRl1ct07ZdO4YNP5qWeXmM+eJzSktLoyzfCDgG
+IE6umiQAUBSbbIR8B6wa5RFs7NzOO+PF3LT7XfQoWOnKEurlsvMzGSbfv0ZcsCBTPpqIvPnzYuy
fBbB/QAlwMgoC6eCAUBSbdEZeB/oE2nRLl2476FHOOTwYWRm+pYXVy3z8jh82HAaNW7M56NGRbmI
UAawF8GMwDtRFU0Ffxok1QabElxL3SzKokcefQz3PfQIXbttEmXZOqO4uJhVq1ZRXFzM8oJ8SktK
KSkpobi4OJYLHGVmZrLdgO3ZZbfd+ezTT1m+vCDK8rsArYE3oiyaTN4EKCndegNvAx2jKtiiRQuu
/fdN7Lt/0rcJSLni4mJm/TCTObNnM3fOHObOnc2CefNZunQJ+fn5FOQvo6CggNWrVm2wVmZmJi3z
8mjZsiUtWuaRl5dH27bt6Ni5Mx07daJTp8506tyFjTbeOAX/stRatXIlV17+F0Y890zUpR8i2Fo4
pfsZh2EAkJROWwNvAe2iKth7iz7cdf+DdOnaLaqSabNwwXy+HD+OqVOmMG3qVL6ZOoVZP8xMxWY4
v9KsWXM279mTHj170bNXL/psuRVb9t26XuyP8ORjj3D1lVdQUhLp2j5PA8cDZVEWjZoBQFK6bEEw
7d82qoIHH3o4/7j+3zRq1CiqkilTWVnJ1CmT+XzUp4wbO4bxY8cwb+7cdLe1Xjk5OfTZciu27b8d
/Qdsz4477UzLvLx0txXK+HFjOeeMU1kwf36UZZ8ATgCSvmNRWAYASemwGfAR0CGKYtnZOVx25VWc
8LtToiiXMvnLlvHxRx/w8Ycf8PEHH7B4cd1dXC4rK4stt+rLrnsMYrfd92Cbfv3Jyqo7t5ktXbKE
c886nc9HfRpl2fuBM6ilWwobACSlWmfgY6BrFMVatGjB3Q88zMAdd4qiXNItWbyYt954nf++9iqf
j/o05dP5qdK6TRv22W8I+w89gB122rlOhIHysjKuvPwvPPX4o1GWvQX4Y5QFo2IAkJRKGxF88t88
imKdu3ThgUeeoPvmkZRLmqKiIt547RWee+Y/jP78s3p70l+fvFatGHrgQQwbfgxb9u2b7nY26L67
7uT6a6+hsjKyD+7XAFdEVSwqBgBJqdKC4JP/VlEU22bbftz38KO1eve+ryZO4JmnnuSVF0dQWLgi
3e3UCr236MORRx/DwYcdQYsWLdLdznq9/uorXPSHcyguLo6q5AXAzVEVi4IBQFIq5ACvAZHsvLPP
fkO4+Y67auWz7OVlZbzx39d5+P57GT9ubLrbqbUaNWrEoUccyUmnnMZm3bunu511GjdmNKf97gQK
8vOjKFcBDCPY1rpWMABISoUHgZOjKHTo4cO47qZbat015VUrV/Lk44/y6MMP1uq792ubjIwMdh+0
J6eecRY77rxLutv5jW+nfcMJRx3JokULoyhXBAwCPo+iWKIMAJKS7XLg6igKHXXs8Vx97XVkZmZG
US4Sq1et4pmnn+TuO25jyeLF6W6nTuu33QDO/P257LX3Pulu5Vdmz5rF8UcNY/asH6IotwTYEfgu
imKJMABISqZjgMeJ4L3mlNPP5NIrriQjo3a8ba1evZpHH36QB+69m/xly9LdTr2y/cAdOP9Pf2bg
Djumu5WfzZ0zh+OPGsYPM2dEUW4qsBPBlsJpUzt+kiTVR9sR3PSX8IX63593PhdcfEniHUWgoqKC
l154jhv+dS0LF0S6cIzWMnjf/bjksivYZNNIt4gIbdGihRw//Ai++/bbKMq9BQwhjUsGGwAkJUMr
YAyQ8C48J51yGlf8LZIrCAn7cvw4rrnqr4wfOybdrcRGdnYORww/igsvvoRWrVunux0WLpjP8MMO
iepywD8ILpGlhQFAUtSyCHZEG5xooWNPOIm//ePatE/75y9bxj/+diUjnn82rX3UwCqCa80lwE/P
HxYDPy3enw00A5oQ7GCXneoGa6pV69ZcftXfOfjQw9PdCnNmz2b4YQdFsXRwJXAo8FLiXdWcAUBS
1K4FEp6vP/SII7n+plvSfsPfiy88xzVX/bW2XeefC0wCpgEzf3zNABYBS4E1NazXgmBDpg4Eszbd
CLZo7g30IdjrvlbYbfdB/P3a6+jcpUta+/h++nSOPuKQKG78XA4MACK5rlATBgBJUToUeJ4E31v2
G3IAt919b1of9Vswfz6XXnQBH334ftp6+NFC4FPgE2A8MIHgJJ8qWUB3oC+wA8HNa/0J1nZIi0aN
GnHhny/lpFNOS+vs0NQpkzn68ENZsWJ5oqUmEfxvu+E9nCNkAJAUlc4EJ6eEtoTrt90AHv/Pc2nd
ava1V17mikv+xPLlCb+xh1EIvENwGeU9asHjYuvQCNge2BfYn2Bb55SfT3bedTduuPlW2m+0caoP
/bNRn4zkd8cdTWlpaaKlHgBOi6ClajMASIpCJvAusEciRbp07cbzL7+Wtpu9VhYWctUVlzHiuWdS
feiFwHMEq8SNJLh2X5dsDBxAsNLdngSzBinRMi+Pf1x3A/sNOSBVh/yNF559hosv+EMUewccTgpX
CqxdS2lJqqsuBk5NpEDLli154tnn6dCxY0Qt1cyUyV9z3PAj+HzUJ6k6ZCHBGgl/As4jWCp5Bml8
LCwBK4FxwGPA3QT/jtZAp2QfeM2aNbz+ysssXbKEXXbbPS2XjXr36UMlRLGV8GDgCYLvjaQzAEhK
VD+CN63Q7ye5ubk88OgTbLlVenaKG/H8s5x16u9YumRJKg43GvgbwdLIzxGcLGvlfvEhrSZ4BPRB
gvtByoAeJPlGwq8mfMnIjz5k90F70rRZs2Qeap0G7rAjs2fNYuqUyYmUaUxwr8UTpOB7wksAkhLR
GBgL9EqkyDX/up6jjzshmo5qoLS0lH/87a889n8PJ/tQ5QRTuzdSS9aBT7HGwIkEO+Ildeef1m3a
cOud96RlX4Hi4mKGH3oQX02ckGipiwi+V5LKACApEf8GLkykwBHDj+K6G2+JqJ3qW758OWefdjKf
fZrUKf9i4H7gJoJP+nGXCRwMXErw6FtSZGVn8/d/XMtRxx6frEOs19w5czh4yD6JPja6BtgG+Caa
rtbNSwCSwtqGYJo39IP6W/TZkrvuf4js7NSuQxNs7nIEEyd8maxDlAJPAkf8+GtBsg5Ux1QSrIN/
P8FjjVsS3EAY7UEqKnjvnbdZXlDArrvvkdJHBZs3b85WfbfmpRHPJ3JTYDbBpYBHouvstwwAksLI
AV4nWDgmlLxWrXjimedp1apVdF1Vw/hxYzn+qGHMnTMnGeUrgWeAQ4CH8cRfle8JgsAUgtmAllEf
YML4cUz/dhp7Dt4npSGzc5cu5OTm8unIjxMp0xWYR3CJLSkMAJLC+DNwbNjBGRkZ3HHvA/TdepsI
W9qwTz7+iFNOOJYVyXm+fywwnGC63xN/9X0N3Esw7T0QyI2y+LfTpjF29BfsN2QoubmRlq5S/wHb
M3HCl8yckdCVn10JnqxIylMBtWdTbUl1RQ/gr4kUOO7Ek9hjz70iaqd63n37LU476XhWr14ddenl
wJkEC+OMjLp4TBQB1wA9gRejLv75qE858ZjhKV3YKSMjg+tuvIXWbdokUqYlcHtELf2GMwCSauop
gjfqULr36MGd9z6Y0inZ1155mfPOOoPS0sjX13mJYEvXD6hfj/KlSyHwH4JZgd2AplEVXjB/Ph9/
+AH77j+Exo0bR1W2So2bNKH75j145aURiZTpTfDoaOR7BTgDIKkmDgT2Djs4NzeXm2+/i4YNG0bY
UtVee/kl/njOWZSVJbxU6y/lA0cRXOufF2VhAcH6CFsQ3EAZmclfT+LYIw9P6cZOg/YazHEnnpRo
mRtJwt4LBgBJ1ZVL8NhfaBf86RK26LNlRO1s2DtvvckF551DeXmki+u9T7D2/X+iLKrfyCe4z+Q4
gssskfh22jeceOzwKDbwqbZLLr+S7ptvnkiJXsDvI2rnZ64DIKm6LiCBxUn6br0Nz738WsqWah35
0YecdtLxlJRENu1fDlwOXA9URFVU1dINeJrgJsFI9NtuAI888TSNmzSJqmSVxo0ZzfDDDqaiIvS3
Tj7B/TeRLVfpPQCSqqMtwbRsqLn77OwcHnj0cdq1ax9tV+sxbsxoTjnhWIqLi6MquZhguv8xvNaf
DgUE/9u3I9iKOGHz581jwpfjOfDgQ1MSSjfu0JFlS5cw8cvQa080Irgn4vWoejIASKqOGwgeSQrl
zN+fy0GHHBZhO+v3w8wZHH/UMAoLI3tyagzBJi1JWzVI1VIOvArMIdiGOOG7SGfPmsXcuXPZe9/9
UrJY0HYDBvLiC8+xcuXKsCW2JdhfYXEU/RgAJG1IV4JFbUK9X2yy6Wbcetc9Kbnrv6CggOOHH8H8
eZHdlzcCOIgIp12VsPHAOwRLCic8fz918tdkZmYycMedEm5sQ3IbNGCz7pvz8ojQO/5mEsyCPBtF
PwYASRtyIwms237HPffTbZNNI2xn3UpKSjj1hGOZ9NVXUZW8CTgNiPzZQSVsDsF6AfsDCS8l+fln
o+jSrRu9em+RcGMb0m2TTflmyhSmfxf6qb7eBI+fLky0F58CkFSVzQh2cQtln/2GpGxXtr9d/he+
+PyzKEpVAn8g2OTIm/1qr2+BHQku0SSksrKSSy+6IJl7Q/zKpVf8NZFVCTOBq6LowwAgqSpXEvJa
a05ODn++7PKI21m3Ec8/y9NPPh5FqXKCT/23RVFMSbcEGAS8l2ihkpISzj7tZJYtXZp4VxvQuUtX
Tj7tjERKHEKw8mRCDACS1qcXcEzYwb877fSUTP1PmjiRyy6+KIpSZcDxBDscqu5YSbBA1duJFpo/
bx5/PPfsqNeNWKezz/0Dbdu2S6TElYn24D0AktbnBqBfmIFt2rbljnvuJ7dBg4hb+rWCggKOG344
+fn5iZYqJzj5P514V0qDUoIb43YEEkqds374gYqKiqRfusrNzaVlXh7vvPVG2BKbAy8DC8IWcAZA
0rp0IIFP/+eefyFNmzWLsJ11++ulF0exrW8lcDqe/Ou6NQRT458mWujuO26L6n6SKh027Eh69OyV
SIkLExlsAJC0LucQclvWjp06MfyY0DsFV9uI557htVdejqLUecBDURRS2q0EhhI8KhhaRUUFF573
+6QvF5yZmcn5F/0pkRJHAp1DHz+RI0uql5oQfCIO5Zw/XEBOTuT7lvzK7FmzuOqKy6IodR1wRxSF
VGsUEISAHxIpMm/uXK667C/RdFSFffYbQt+ttwk7PIcgwIZiAJC0tpOB1mEGdunajcOGDYu4nV+r
rKzk4j+ex8rEV/p7Bkj+O7zSYT5wAAluIvTSiOd5/dVXouloPTIyMjjn/AsSKXE60DzMQAOApF/K
BM4PO/i8P15AdnZyP/0//cRjUVyf/YxgfQOf86+/JgHDCJ7uCO3vV1zG8uXJvRSw1977sPU224Yd
3pzg0dUa8ykASb+0L3BumIGdOnfmn9ffSGZm8j5XLFy4gDNP+R0lJQlt8rMA2ItgdzXVb98DRcA+
YQusXr2K5QUF7LV36BLV0rpNW1596cWww7sBd9Z0kDMAkn4p9LX/k087g6wkr/f/t8svo7BwRSIl
yoCjgMg2C1CtdyPB5Z7Q/vPUE4z6ZGRE7azbXnvvQ/fNNw87vAewW00HGQAk/WQjguumNda8eQuO
OPKoiNv5tQ/ff483//taomUuBj6MoB3VHZXAKcDU0AUqK/nbFZdRXpbQ1YQqZWRkcNIpoWbyf1Lj
wQYAST85ieCu4ho7+rjjadK0abTd/EJ5WRnXXv23RMu8BdwSQTuqe1YS3A+wJmyBb6d9w1NPRLLc
9HodNmw4bdq2DTv8cCCvJgMMAJIAMgju/q+x7Owcjj8p1NBqe+KxR/l22jeJlFhCEHAqI2lIddEk
4IpECtx8w78oKCiIqJ3fatCgAcccH3rvrYZAjRbgMABIAtiDYGnRGtt3yBA27tAh2m5+YcWK5dx2
878TLXMKwaNhirebgHfDDi4oKODu22+NsJ3fOub4ExJ5kubUmnyxAUASJLDs71HHHBdlH79x3113
kr9sWSIlniRYM12qILhWvipsgUcffpB5c+dG19Fa2rZtl8gTB1sDfar7xQYASTnAoWEGdu7SlR12
2jnidv6nID+fR/8voVV6lwJ/jKgd1Q8zSGAnvZKSEu6+I7m7RSe4lPaR1f1CA4CkvQi58t9Rxx6X
1Of+7737DlatXJlIifOBRRG1o/rjFmB02MHPPv1kFJtQrdeuu+9Bh44dww43AEiqtuFhBmVlZ3PY
sGq/19RY/rJlPPHoI4mU+AR4IqJ2VL+UA2cSciXI0tJS7krivQCZmZkcMfzosMN7AX2rdZywR5BU
L+QCB4cZuOfgvWnXrn3E7fzPQ/ffm8in/wqCFQ2961/rMw54LOzg5595moULkndf6bDhR5ORkRF2
eLWSuQFAire9qeGzwz854MBQuaFaioqKePLxRxMp8RAJbgmrWPgLwRoBNVZaWsqjDydvF+kOHTvS
b7sBYYcbACRt0IFhBjVq1Ig9B+8ddS8/G/HcMxTkh16qfxUJPu+t2JgHhH7G9OknHmP16tURtvNr
Qw88KOzQzQkuBVTJACDFW6jnjQbtNZjGTZpE3QsQLLv6yEMPJFLiDoINf6TquJngaZEaKygo4Pln
/hNxO/+z/wEHkpUVes++/Tb0BQYAKb56A5uEGbj/AaE/mWzQxx99wHfffht2eCEJfKJTLK0gge+Z
Rx9+gMrK5Nxq0q5de/qHvwxgAJC0Xht8g1iXxo0bM2ivwVH38rNnn3oykeG3ESz7K9XE7cDiMAO/
nz6dsaO/iLid/0kgbO8GNKrqCwwAUnyFCgA77bobjRpV+b4SWkF+Pu+89WbY4cUEb+RSTa0C7gw7
+Nn/PBVhK7+2976hfkwhOPnvXtUXGACkeGoE7Bpm4O57DIq4lf954blnKCkpCTv8EWBhhO0oXm4n
5BLBr7/yMisLCyNuJ7Bxhw5079Ej7PAq04MBQIqnndjA9OD67LbHnhG38j8J3FBVSXAzlxTWMiDU
s6erV6/mv6+9GnE7/7N7+J+5KgcaAKR42inMoM26d6dT585R9wLA9O++Y+qUyWGHvwVMjbAdxVPo
S0ivv5K8/aZ2Cz/r1gdosb6/NABI8RRqB5/dB+0VdR8/e/2VlxIZfn9UfSjWpgAjwwz89JORiaxd
UaXtd9gx7GO3mcCOVf2lpHjJBAaGGbjLblXeU5SQN//7etihC3G7X0UnVJgsKytN5AbWKuXm5jJg
+1A/slDFbJ8BQIqfLYCWNR2UmZnJtv37J6EdmDnje6ZM/jrs8EeA0gjbUbw9BywPM/C/r70ScSv/
03/A9mGHrne2zwAgxc8uYQb16NmL5s3XezkxIe+983Yiwx+Pqg8JWA2MCDNw1CcjKSoqiridwHbh
A8BAIHtdf2EAkOIn1NJi2/RLzqd/gI8//DDs0KnAVxG2IgE8E2ZQcXExoz8fFXUvAGy19TZkZ+eE
GdqEYNbvNwwAUvxsHWbQdgNCL0lapQTfNJO3ELvi7B1C7g+QQJitUuPGjem9xTrP49Wx1br+0AAg
xUsWwR4ANbZt/+0ibiUw5ovPE5k2fT7KXqQflQKhHkv56MP3I27lfxK4D8AAIInNgMY1HdS8eQu6
dgu1b9AGjfok1FNXALNx+l/J898wg76bNo3FixdF3QsAW/btG3aoAUASod5BevbuRUZGRtS9ACSy
kUqoN2ipmt4i5NMl48eOibiVQM9eoSbvwAAgifW8EWxIAm88VSovK2PSVxPDDjcAKJlWAJ+HGTgu
SQGg++Y9yMpe5w39G9KZdTz6awCQ4qVPmEE9evaKug8Apk6ZwurVq8MMrQCSd7FVCoR6PnXcmOQE
gNzcXDbZJPSluC3X/gMDgBQvm4YZlKwZgPHjxoYdOomQi7VINfBJmEGTJk6gtDQ5a1Ml8LP4m599
A4AUL13DDOrRKzkzAAms/vdplH1I6/E5UFbTQcXFxcz4fnoS2kloNu43P/sGACk+mgOtajqoZV5e
0lYAnPZN6A38Qj86INXASuDLMAOnTU3O5pSdu4bK8ADd1v4DA4AUH93CDErW9r+VlZV8N21a2OGh
rx1INRTqgv60ad9E3QcAnTqF/nnstvYfGACk+OgWZlCnTl0ibiMwb+5cVqwIdRm/CPg24nak9Qm1
1kQCs1tVSiCQd1v7DwwAUnyEmjvs2KlT1H0AJHKNdDJQHmErUlVCBYAZ07+Lug8A2rZrT25ubpih
nQlWAv2ZAUCKj43DDEpWAJg7d07YoaEXDpBCCBUA5s6ZQ2VlZdS9kJmZScdwlwFygDa/qhVJR5Lq
gtZhBm3coUPUfQAwd/bssEOd/lcqFQA1Xtu3qKiIZUtD7Se0QQn8TP7qPcAAIMVHmw1/yW+1ahUq
N2zQ3DmhZwBmRNmHVA0zwwyaMyd0yK1SXl5e2KEGACmmavwIIEDLlqHfbKo0d27oN8eZEbYhVcf3
YQYlEHKr1DIv1I8yGACk2Ar1Ub5l+E8bVUpgenRmhG1I1RFq1ilZlwASD+BnjgAAIABJREFU+Jk0
AEgxVeNLABkZGbTM+80eIpFYtmxZmGHlhLgeKyVoQZhBBfmhvsc3yEsAkmqqxmfypk2bkZ2dE3kj
lZWVrFixIszQfIKNgKRUCvVRPj8/P+o+gIRmAH410AAgxUeDmg5o0rRJMvqgsHAF5WU1XmIdYEnU
vUjVEOr7bnlBQdR9ANCsabOwQxv+8jcGACkesgnx856bW+PMUC2FKwrDDk3ORVWpaqG+7woLQ81y
bVBOuIWAAH410AAgxUOod4zc3Oin/wFKSorDDl0ZZR9SNa0KM6ikuCTqPgDIyQn9c2kAkGIo1Ef5
nJzQnzSqlMBe6cl5R5WqFiqxlpQm59s15FLAYACQYinkDECSAkCJAUB1Sqjvu5KSJAWABqEvzf1q
oAFAiodQc4bZ4acaq1RaFjoAhB4oJSDcDED4S11VSiCYGwCkGMra8JesY1Bmct4iKspDb+bnLoBK
h1DfdxUV0W8GBMGGQGGHrvc3kiQpHgwAkiTFkAFAkqQYMgBIkhRDBgBJkmLIACBJUgwZACRJiiED
gCRJMWQAkCQphgwAkiTFkAFAkqQYMgBIkhRDBgBJkmLIACBJUgwZACRJiiEDgCRJMWQAkCQphgwA
kiTFkAFAkqQYMgBIkhRDBgBJkmLIACBJUgwZACRJiiEDgCRJMWQAkCSpauVhBpWWlETdx491S8MO
/dW/wwAgSVLVVoYZtHjRwqj7AGBR+LqFv/yNAUCSpKqVA6trOmj58uWsLCzc8BfW0JzZs8MONQBI
klRDNT7rVlZW8uEH70feyEcfvBd26Kxf/sYAIEnShk0NM+iN116JtInly5fz6ciPww7/9pe/MQBI
krRhX4UZ9Mbrr/HN1CmRNXHvnbezZs2asMMn/vI3BgBJkjbsgzCDKioquPrKKygvD/Ugwa98P306
jzz0QNjhs4Hpv/wDA4AkSRv2KVAUZuCoT0Zy/T+vSejgK1Ys5/TfnZDIp/931/4DA4AkSRtWBLwc
dvAD997NDdf+g4qKihqPXbhgPicePZwZ30/f8Bev33/W/gMDgCRJ1fNoIoPvufN2Tj3xOGb9MLPa
Y955600OGbIfEyd8mcihFwLvrP2H2YlUlCQpRt4kuJN+87AFPnz/PfbZY1eGHXUMBxx0MNttP5Cs
rKxffU1BQQEfvv8uTzz6CGNHf5FgywDcCZSt/YcGAEmSqqcc+BfwYCJFSktLefKxR3jysUdo1qw5
HTp2ZOMOHVi9ahVLlizmh5kzI7lp8EeFwB3r+gsDgCRJ1fcYcBHQO4pihYUr+GbqikgfFVzLdUD+
uv7CewAkSaq+UuBMoDLdjVTDd8CN6/tLA4AkSTXzEXB/upvYgDLgZGC9zw0aACRJqrk/AOPT3UQV
rgKqXDPYACBJUs2tAY4AFqS7kXV4Brh2Q19kAJAkKZzvgf2B5elu5BfeA04ANrjikAFAkqTwvgR2
BealuxHgbeAQoLg6X2wAkCQpMV8BuwGT09jDvcAQguf+q8UAIElS4qYD2wP/l+LjFgLHETya+JvV
/qpiAJAkKRqrgN8Bg4GpKTjeq0Bf4Ikwgw0AkiRF612gH/BHknNvwIcEIeNAYGbYIgYASZKiVwTc
AmxKcFf+2wR7CYSVT7D40I7AHgQhIyHuBSBJUvIUE+wf8BjQDtgTGARsA/QEWqxjTCUwB5gGfAq8
/+Ov1bq7v7oMAJIkpcYi4OkfXz9pAbQCmgIlwEpgGcEMQlIZACRJSp/lpGkhIe8BkCQphgwAkiTF
kAFAkqQYMgBIkhRDBgBJkmLIACBJUgwZACRJiiEDgCRJMWQAkCQphgwAkiTFkAFAkqQYMgBIkhRD
BgBJkmLIACBJUgwZACRJiiEDgCRJMWQAkCQphgwAkiTFkAFAkqQYMgBIkhRDBgBJkmIoI8n1ewG7
A32BHkBXoBXQBGiY5GNLEkA5sOLH13xg6o+vkcAXQGn6WpPSJxkBYDvgBOBwoEMS6ktSVFYCbwGP
Aa8DJeltR0qdqAJAJjAMuATYJqKakpRKi4FbgTuA5WnuRUq6KALAvsAtBNP9klTXLQeuBm7DywOq
xxIJAG2Auwg++UtSfTMJOIXgPgGp3skKOW57gutmO0fYiyTVJu2A3xG8T36Y5l6kyIUJAKcAIwju
5pek+iwT2APoDrxK8ESBVC/UdB2AS4D7gewk9CJJtdWxwMsEjzBL9UJN7gE4B7g9WY1IUh3wHjAE
KE53I1KiqnsJ4FiCT/7JXjhIkmqzTX58jUh3I1KiqhMAtiSY+spNci+SVBf0BQqAz9LdiJSIDX2i
bwqMAXqmoBdJqitKgF2A0eluRAprQzcBXoUnf0laWy7wMJCT7kaksKqaAdgKGEcS7vjPa5FLXvMG
UZeVVE0VFZUUripl9ZpyysoryAByc7Jo2jibxo1S/5BPeXnQT9GaMsrKK8nMzCA3J5OmjXNo1DDs
ciWBouIyFi5ZQ0VFZUTd/sqFwE3JKCwlW1UB4BXggEQPkJmZwW4D2nPwPl3Yd9eOdO3YNOEfaEmq
ifLyShYsKeLj0Qt56a1ZvPb+HFaujmSV3wKgG+4doDpofQFga2B8FX9fLYN37sC//tyffn1aJ1JG
kiK1tKCYf983iVsenkxxScJr+1wG/DOCtqSUWt8J/jHguLBFG+Rmcfc1O3LS4d3DlpCkpJs4NZ9D
zniXmXNWJlJmEdAZtxJWHbOuANAcmA80DlMwr0UuL98/mJ37t0uoMUlKhcXL1nDw6e/y2fjFiZQ5
BHgpopaklFjXUwCHE/Lkn52VydO37eHJX1Kd0bZVQ166by+6dWqaSJnQM6ZSuqwrAAwNW+zai/uz
9y4dEmhHklLvpxCQm1PT7VF+ti/ukaI6Zu3v9gxg9zCFemzSnPNO6p14R5KUBlv1zOOsY3uFHd4M
GBBhO1LSrR0AegNtwhS65sJ+5GSHTs+SlHaXndOXZk1Cr+2za5S9SMm2rgBQYy2b53Lw3l0iaEeS
0qdNXkMO2LNz2OGhpw+kdFg7AIT6Bj5wr85++pdULxy0d+gA4LLpqlPWPmtvHKbITt71L6me2GW7
9mGHdoqyDynZ1g4AzcMU6dg+1FODklTrbNSmEVlZoRZBbRZ1L1IyrR0AQj0Iu1HbRhG0Iknpl5WV
QbvWDcMMNQCoTlk7AITapadBrpv7SKo/GjYI9Z7mOgCqU7xzT5KkGDIASJIUQwYASZJiyAAgSVIM
GQAkSYohA4AkSTFkAJAkKYYMAJIkxZABQJKkGDIASJIUQwYASZJiyAAgSVIMGQAkSYohA4AkSTFk
AJAkKYYMAJIkxZABQJKkGDIASJIUQwYASZJiyAAgSVIMGQAkSYohA4AkSTFkAJAkKYYMAJIkxZAB
QJKkGDIASJIUQwYASZJiyAAgSVIMGQAkSYohA4AkSTFkAJAkKYYMAJIkxVB2uhuQ1rZqdRkz565k
xuxC5i8qYvWaMtYUlwOwenUZzZvl0qplLq1bNqRVy1y6dWpKp42apLlrSapbDABKqxUrSxk1bhGj
xi9m1LhFjP96GUvy19S4TquWDdiqZx5b9cxj+63bsM+uHWnXumESOpak+sEAoJSbv6iI596YybOv
zeTTcYuoqKhMuOaygmI+/HwBH36+AIDMzAy23aIV++3ekUP37Uq/Pq0TPoYk1ScGAKVEaVkFz74+
k/ufnsbHoxdGctKvSkVFJWMnLWXspKX8486J9OvTmlOGb84xB21Ki2a5ST22JNUFBgAl1YqVpdz3
1Dfc/sgUZs9flbY+xn29lHF/Xcqfrh3DKcM255Kz+rJxu0Zp60eS0s2nAJQUJaUVXH/fJLru8iwX
/2tMWk/+v7S6qIzbH51C90HPcdE/R7Noac3vN5Ck+sAAoMi98u5sttz3RS65bgzLC0vS3c46Fa0p
56YHv2bzQc9z2yNTkn5JQpJqGwOAIrNwSREHnfYuB5/+Lt/9sCLd7VRL4apSzv/75+xy5Ot89U1+
utuRpJQxACgSb3w0l22Gvsyr781OdyuhfDZ+Mdsd9ArX3fsVlU4GSIoBA4ASUlpWwYX/GM3Qk99m
4ZKidLeTkNKyCi69fiyHn/UeK1aWprsdSUoqA4BCW7m6lINOe5ebH/q6Xn1qfvHtWWx/yCtM/q4g
3a1IUtIYABTKgsVFDDr6Dd78aG66W0mKaTNWsMuw1xk1fnG6W5GkpDAAqMamzypk52GvMXbS0nS3
klQFK0rY78S3+ODH1QUlqT4xAKhG5i5czT4nvMmM2SvT3UpKFK4qZejJb/PWx/PS3YokRcqVAFVt
SwuK2feEt5J+8s/JzmTzTZqzaZdmbNq5KXktGtC8aQ6NG2VTXl5J/vJi8leUsHBJEZO/LWDq9OUU
rSlPWj9Fa8o54uz3+eDp/dxTQFK9YQBQtaxaXcbQk99O2o1xm3VtxkGDO7Nz//YM6Nuaxo2q/61Z
UVHJ5G8LeHfUAt79ZB6ff7kk8oV9Vq4u5cBT3+HT54bStWPTSGtLUjoYAFQtZ17+KV9MWBJpzYYN
sjjqwE045qBN2bZPq9B1MjMz2LJnHlv2zOMPJ/Vm9vxVPPHi9zzx0vcsWBzdo4nzFxUx9OR3+PT5
oTRvmhNZXUlKB+8B0Abd+9Q3PPHS95HVa9Qwi/NP3oLxrx7IDZdul9DJf106b9yES87airEvH8i1
f+rHRm2j2/Rn8ncFnHPlZ5HVk6R0MQCoSuMnL+OPV38RWb0DB3fm0+eHctnv+9KmVcPI6q5Lbm4m
px7Vg9EvHcD5v9uCnOxovt0ff3E6j784PZJakpQuBgCtV0lpBcee/yFrihO/wS6veS7/9+9deOi6
nem0UeMIuqu+hg2yuOycvrz92D706dEykprnXPkZ02cVRlJLktLBAKD1uuG+SUydvjzhOgO2bsP7
T+/H0EGdIugqvD49WvLG/+3NUQdsknCtFStLOfuKURF0JUnpYQDQOs2YvZJ/3jUh4ToH7NWZEfcM
omP71H7qX5+GDbK4/W8DuebCbcnISKzW2yPnMeKtWdE0JkkpZgDQOv3h758n/Gz9SUd058F/7USD
3KyIuorOGcf05Na/DiQrM7EUcNE/v4jkEokkpZoBQL8xavzihLf1PXJoN677c38yEzzBJtPRB23C
LVdun9BMwIzZK7nl4cnRNSVJKWIA0G9ce9fEhMbvs2sHbr1yYK0++f/kqAM24eIztkqoxi0PfZ3U
lQglKRkMAPqVCVOW8dr74T/9d+vUlLuv2ZHsrNp/8v/JRaf14ZB9uoQev2jpGv7v+W8j7EiSks8A
oF+5/t5JVIZcRbdBbhYPXb9znVwl78bLBtC5Q5PQ42964GvKy6NdfliSkskAoJ8VrChhxFs/hB5/
wSlbsFXPvAg7Sp3mTXO4++odQt8PMH1WIa9/MCfapiQpiQwA+tkzr88MfUf7pl2acc4JvSPuKLUG
btOW4UPDrxHw1MvRLZcsSclmANDPHh8Rfnnbf17Uj9zcuv/tdPm5fWnaONweWa+8N5uVq0sj7kiS
kqPuv2MrErPmreKTsQtDjd22Tyv22nnjiDtKj/ZtGnHykZuHGrtqdRkvv5PY45OSlCoGAAHw7qfz
Qt/8d/7JW0TbTJqdfnSP0LMZL7/jyoCS6gYDgAD44LMFocZ1aN+Y/XbrGHE36dW+TSMO26drqLEf
fRFuFkWSUs0AIAA+/DxcABi2f9c6seBPTR0xpFuocQsWF/HtzBXRNiNJSWAAEDNmr2TWvFWhxh6+
f7hPyrXdLgPa0a51w1Bjw4YpSUolA4CYNC0/1Lj2bRrRu3vLiLupHbIyM0Lf2Dh64pKIu5Gk6BkA
xLQZ4aasd9muXcSd1C47bhvu3zf9h8KIO5Gk6BkAxLczlocat/02bSLupHbZsV/bUOO++8F7ACTV
fgYAhb5pbfNuzSPupHbp2rEpjRpm1XjcnAWrKS5xd0BJtZsBQCxauibUuM26NIu4k9olIwO6dWxa
43EVFZXMnh/upkpJShUDgFi5uqzGY7KzMti4XeMkdFO7dAkRAABWrqr5/6aSlEoGAFG4qubr1zdp
nBN657y6JOzWxkUhN1WSpFQxAIiVoQJAuA1z6pow9wAAFK1xBkBS7WYAEKVlFTUeE4MP/wDk5oYL
AKuLDACSajcDgGgQ4iRXUFiShE5qn+Urwv07c3PCBQdJShUDgGjYoOYnq9VFZaFmDuqaJfnhnpBo
2iQel0gk1V0GANG6ZYMaj6mshBWFNb93oK5Zml8calyLZrkRdyJJ0TIAiI3aNgo1bubclRF3UvvM
W1QUalzH9vX/EUlJdZsBQHTcKNzJavzXyyLupHaZPX8VS5bV/BJAk8bZtGzuDICk2s0AIHp0axFq
3ITJ9TsAjP1qaahx9X2JZEn1gwFA9Ng03Alr/ORwJ8i6YuykcP++LerpFsmS6hcDgNi2T+tQ4775
fgUzZtff+wA++GxBqHFb9cqLuBNJip4BQGzRvWXoa9YvvPlDxN3UDhOn5jN1erhtknfq1y7ibiQp
egYAkZEBA7dpG2rsiHoaAJ59fWaocbk5mWy3VZtom5GkJDAACIDBO3cINe6b71eEvlZeW5WUVvDC
G+GCzc7924XeP0CSUskAIAD2271j6LE33v91hJ2k32MjprNoabgVAA/Yq3PE3UhSchgABECfzVvS
Y5NwTwO8PXJevZkFWFNczi0PTQ41NiMDDhrcJeKOJCk5DAD62VEHbhp67HV3fxVhJ+nz8LPfsWBx
uNX/Bm7Tls26NIu4I0lKDgOAfnbswZuSEXKf3/c/W8Bz/63bNwTOWbCaf98/KfT4Yw/eLMJuJCm5
DAD62ebdmoe+GRDgz/8aw9yFqyPsKHUqKio598rPWLEy3AZHTRvncNwh4WdQJCnVDAD6lbOP7xV6
7IqVpfzx6i+orIywoRS598lpjByzKPT4Ew/fzB0AJdUpBgD9yoF7dk5oKdv3Ry3gmjsmRNhR8n30
xcKEes7OyuT8k/tE2JEkJZ8BQL+SmZnBZef0TajGbf83hXue+CaijpJr8rcFnHzxJ5SUVoSucdwh
m3rzn6Q6xwCg3zhyyCZss0WrhGpcecuXjHhzVkQdJceM2Ss54uwPWF5YErpGo4ZZXH7u1hF2JUmp
YQDQb2RlZXDT5dsnVKOiopIzLvuU6+8Nf1d9Mo35ailDT36HxcvCLfjzk0vP7sumnf30L6nuMQBo
nfYYuBFH7N8toRqVlXDDfZO46J9jKCuvPXcGjnhzFoec/l7CJ/9em7XgT6dtGVFXkpRaBgCt121X
DqRVywYJ13nk+e84/Kz3mTknvVsHF60p5683j+eMyz6luKQ8oVoZGXDX1TvSINd1/yXVTQYArddG
bRtxyxWJXQr4yadjF7Hb8P9y52NT0zIb8NEXC9lt+H+5+/FvInlM8cTDurPHwI0SLyRJaWIAUJWO
O2QzTjg0mhXuitaUc9UtX7LfiW/z3Q+FkdTckNVFZfzx6tEccXZ0MxA9NmnOrVcOjKSWJKWLAUAb
dMffd0hobYC1TZiyjMHHvck7n8yLrOa6LFhcxN7Hv8XjL06PbHGiRg2z+M/te9CsSU40BSUpTQwA
2qCmjXN4+YG9aJPXMLKaq1aXcfqlo5iepJmA0rIKTv7zJ0ybsSKymhkZ8H837MrWvRN7RFKSagMD
gKpl087NeP7uQTRqGN1Nb4WrSrnmzomR1ful59/4gdETlkRa8+oL+jFsSLdIa0pSuhgAVG27DmjP
M3cMIjcnum+bNz6Yk/DjeOvy6PPTI6134al9+MvZia2QKEm1iQFANTJ0UCeevHX3yEJAWXklk79d
Hkmtn1RWwqRp+ZHV+/3xvbj+kgGR1ZOk2sAAoBo7bN+uvHjfXjRulB1JvbkLVkVS5yf5y4spWpPY
c/4/ufSsvtx+1Q5kZERSTpJqDQOAQtlvt46889i+bNS2UcK1ol4XIIp62VmZ3HblQP5xUb8IOpKk
2scAoNB22LYtn484gH59Wqe7lUi1atmA1x4azDkn9E53K5KUNAYAJaTzxk345LkhnHdi73oxTb79
1m0Y/eKB7L1Lh3S3IklJFc1FXMVag9wsbvnrQHJzs/j3/bVz97/q6LN5S0Y+O4TsLHOxpPrPdzpF
pmXz3HS3kJDmTXM8+UuKDd/tJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgyAEiSFEMGAEmSYsgA
IElSDBkAJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgyAEiSFEMGAEmSYsgAIElSDBkAJEmKIQOA
JEkxZACQJCmGDACSJMWQAUCSpBgyAEiSFEMGAEmSYsgAIElSDBkAJEmKIQOAJEkxZACQJCmGDACS
JMWQAUCSpBgyAEiSFEMGAEmSYsgAIElSDBkAJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgyAEiS
FEMGAEmSYsgAIElSDBkAJEmKIQOAIpOVlRFqXFl5RaR9lJWFq5eZGa5/SaqLDACKTJNG2aHGLV66
JtI+FiwpCjWuWZOcSPuQpNrMAKDINGsa7gQ6a96qSPuYHbJe2P4lqS4yACgynTduEmrcO5/Mp7yi
MrI+3h45L9S4Lh3C9S9JdZEBQJHpuWmLUOOWFRTz4WcLIulh5eoy3vwoXADosUm4/iWpLjIAKDId
2jWmTV7DUGP/eddXVEYwCXD7I1MoWFESamzfXnmJNyBJdYQBQJHJyIA9dtgo1NgJU5Zx1+NTEzr+
hCnLuOuxcDWaN82h/1atEzq+JNUlBgBFavAuHUKPvfq2Cbz+wZxQY+csWM0JF45kTXF5qPGDdtiY
7Cx/HCTFh+94itTh+3UlNyfct1V5RSUn/+kT7n78mxpdDvhs/GL2Of4t5i1cHeq4AMMP3CT0WEmq
iwwAilTrlg0YOqhT6PHlFZX89ebx7H/S24wcs6jKIPDD3JWcc+VnHHz6eyxeFn4tgRbNcjl4cJfQ
4yWpLgq3cotUhT+e0ocRb81KqMbYSUs59Iz36NC+MYN33phNOjejbauGrFhZwryFq/noi4VMnJof
Sb9nHduTRg2zIqklSXWFAUCR22W79uw+cCM+/DzxR/vmLVzNoy9Mj6CrdWvcKJvzT+6TtPqSVFt5
CUBJ8e+/DAi9N0AqXXLWVrRrHe7RRUmqywwASor+W7bm9KN6pruNKvXYpDl/Om3LdLchSWlhAFDS
XH/JdvTuXjtX18vJzuThG3alQa7X/iXFkwFASdOkcTZP3boHjUPuEphMN/xlADtu2zbdbUhS2hgA
lFR9e+Xx4r17hl4bIBnOPq4X553YO91tSFJa1Z53ZdVbg3fuwEPX71IrVto78bDu3HblwHS3IUlp
l/53ZMXCMQdtygv3DErr5YDzTuzNg9ftTGZm7X86QZKSzQCglDlgz858PuIAtujeMqXHbdQwi5uv
2J5b/jrQk78k/cgAoJTqs3lLRr0wlNOP6pGSk/GO27Zl9EsH8oeTtkj6sSSpLjEAKOWaNcnhnn/s
xMhnhrBTv3ZJOUanjZpw/7U7M/LZoSmfcZCkuqD2PZ+l2Nhh27aMfHYI742az00PfM2bH8+lvLwG
2wCuw1Y98/j9Cb058bDNfMZfkqpgAFDa7bnjxuy548YsWFzEM6/P5J2R8/joiwWsWFm6wbHZWZkM
6NuaPXfamCP278bWvVuloGNJqvsMAKo1NmrbiPNO7M15J/amrLyCmXNWMnX6cmbNW0XhqlKWryih
SeMcmjbJZqO2jeixSQt6btK8Vi40JEm1ne+cqpWyszLp3rU53bs2T3crklQveROgJEkxZACQJCmG
DACSJMWQAUCSpBgyAEiSFEMGAEmSYsgAIElSDBkAJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgy
AEiSFEMGAEmSYsgAIElSDBkAJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgyAEiSFEMGAEmSYsgA
IElSDBkAJEmKIQOAJEkxZACQJCmGDACSJMWQAUCSpBgyAEiSFEMGAEmSYsgAIElSDBkAJEmKobUD
QHmYIsUloYZJUq20pjjUe1pZ1H1IybR2AFgZpsj8RUURtCJJ6VdeXsmipWvCDC2MuhcpmdYOAKG+
gectWh1BK5KUfguWFFFeXhlmqAFAdcraAWBemCKfjFkUQSuSlH4fj14YdujcKPuQkm3tADA1TJFX
35tNSWlFBO1IUnq9/PassEO/ibIPKdnWDgBTwhQpWFHCy++E/qGRpFphSf4aXnt/TtjhoT5ASemy
rgCwJEyhy28cR2mZswCS6q5rbp9A4arSsMM/jrIXKdnWDgCVwIdhCk2bsYJbH56ceEeSlAYTp+Zz
z5OhZ/ELgdERtiMl3boWAnotbLG/3DCOdz4JdR+hJKXNkvw1HHrmu4ncy/QWEHrqQEqHdQWA54FQ
z/WVlVcw/NwPGDV+cWJdSVKKLMlfw8GnvcuM2aGWQfnJY1H1I6VK1jr+rBjoDfQNU3BNcTlPvPg9
nTs0YZverRJqTpKSaeLUfAYf/yZffVOQSJnFwNmEXElVSpd1BQCA74EzgIwwRcvLK3np7Vl8MnYR
W/bMY+O2jUI3KElRW1ZQzN9vm8Cpl3zCkvziRMv9E/gg8a6k1KrqBP8KcECiB8jMzGCPgRtx0N5d
2HfXDnTr1JQGuevLHZIUvYqKShYsLmLkmEW89PYsXn1vdiJ3+/9SAdANWB5FMSmVqgoAWwLjgJyo
D9omryHNm0ZeVpJ+o7iknIVL1lBWnpTHlC8Abk5GYSnZNjTFfwNwUSoakaQ6ZhLQD+/+Vx21oQDQ
FBgD9ExBL5JUV5QAu+Cz/6rD1vUY4C+tBI4g5GOBklRPXYwnf9Vx1bkbbxHwA3AoIZ8KkKR65Ang
knQ3ISWqurfjf0WwR8CQJPYiSbXde8BwoCzdjUiJqsnzeKMJrnvtiTMBkuLnLeAQoCjdjUhRqOkD
+SOB2cBQNnz/gCTVF08ARwFr0t2IFJUwJ/GHgJ2BmdG2Ikm1Thmjy2owAAAA5ElEQVTB9f7jCWZA
pXoj7JJ8cwkScVeCBYMkqb75CjgYeCbdjUjJkMg0/hKCKbF9gCnRtCNJaVcAXEiwyI+P+qneiupm
vkyC9QL+AmwdUU1JSqXFwK3AHbi2v2IgGXfzbwecABwGdExCfUmKykqCu/sfA17H6/yKkWQ/ztcL
2APYimA54a5Aa4Ilht0NSFKq5BOc7OcBU398fQx8gWv5S5IkSZIkSZIkSZIkSZIkSZIkSZIkSZIk
SZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIk1TH/D0d3RTQDy9wOAAAA
AElFTkSuQmCC
"""

    image = Image.open(BytesIO(base64.b64decode(encoded_img)))
    icon = pystray.Icon("name", image, "Tooltip")
    icon.run_detached()


class DbusRunner(Process):
    def __init__(self, server):
        super().__init__()
        self.server = server

    def run(self):
        import dbus
        from dbus.mainloop import glib
        from gi.repository import GLib, GObject
        loop = GLib.MainLoop()

        def handle_signal(*args, **kwargs):
            print('Session locked')
            loop.quit()
            self.server.kill_flag.set()

        glib.threads_init()

        glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SystemBus()
        bus.add_signal_receiver(
                handle_signal,
                signal_name="Lock",
                dbus_interface="org.freedesktop.login1.Session"
        )

        show_tray()
        loop.run()


class DmenuRunner(Process):
    """Listen for dmenu calling event and run keepmenu

    Args: server - Server object
          kpo - Keepass object
    """
    def __init__(self, server):
        Process.__init__(self)
        self.server = server
        self.database = get_database()
        self.kpo = get_entries(self.database)
        self.prev_entry = None
        if not self.kpo:
            self.server.kill_flag.set()
            sys.exit()

    @property
    def actions(self):
        return {
            MenuOption.TypeUsername:self.type_username,
            MenuOption.TypePassword:self.type_password,
            MenuOption.TypeEntry:self.type_entry,
            MenuOption.ViewEntry:self.view_entry,
            MenuOption.ReloadDB:self.reload_db,
            MenuOption.KillDaemon:self.kill_daemon,
            MenuOption.Edit:self.edit_entry,
            MenuOption.Add:self.add_entry,
            MenuOption.ManageGroups:self.manage_groups,
            MenuOption.TypePrevUsername:self.type_prev_username,
            MenuOption.TypePrevPassword:self.type_prev_password,
            MenuOption.ShowPrevEntry:self.show_prev_entry,
            MenuOption.TypePrev:self.type_prev,
        }

    def _set_timer(self):
        """Set inactivity timer

        """
        self.cache_timer = Timer(CACHE_PERIOD_MIN * 60, self.cache_time)
        self.cache_timer.daemon = True
        self.cache_timer.start()

    def run(self):
        while True:
            option = self.server.start_q.get()
            if self.server.kill_flag.is_set():
                break
            if not self.kpo:
                pass
            else:
                self.dmenu_run(option)
            if self.server.cache_time_expired.is_set():
                self.server.kill_flag.set()
            if self.server.kill_flag.is_set():
                break

    def type_text(self, data):
        self.prev_text = data
        return type_text(data)

    def cache_time(self):
        """Kill keepmenu daemon when cache timer expires

        """
        self.server.cache_time_expired.set()
        if self.server.start_q.empty():
            self.server.kill_flag.set()

    def dmenu_run(self, option):
        """Run dmenu with the given list of Keepass Entry objects

        If 'hide_groups' is defined in config.ini, hide those from main and
        view/type all views.

        Args: self.kpo - Keepass object

        Note: I had to reload the kpo object after every save to prevent being
        affected by the gibberish password bug in pykeepass:
        https://github.com/pschmitt/pykeepass/issues/43

        Once this is fixed, the extra calls to self.kpo = get_entries... can be
        deleted

        """
        try:
            self.cache_timer.cancel()
        except AttributeError:
            pass

        self._set_timer()

        if option is None:
            option = self.dmenu_select_option()

        if option:
            action = self.actions[option]
            action(prompt=option.description())

    def dmenu_select_option(self):
        selection = view_all_entries(
            [],
            [f'{op.description()}' for op in self.actions.keys()],
            prompt='Select action'
        )

        return next(
            (x for x in self.actions if x.description() == selection),
            None
        )

    def type_entry(self, prompt=None):
        sel = self.dmenu_select(prompt)

        if sel:
            entry = self.get_selected_entry(sel)
            type_entry(entry)
            return True

    def type_password(self, prompt=None):
        sel = self.dmenu_select(prompt)

        if sel:
            entry = self.get_selected_entry(sel)
            self.type_text(entry.password or '')
            return True

    def type_username(self, prompt=None):
        sel = self.dmenu_select(prompt)

        if sel:
            entry = self.get_selected_entry(sel)
            self.type_text(entry.username or '')
            return True

    def view_entry(self, prompt=None):
        sel = self.dmenu_select(prompt)

        if sel:
            entry = self.get_selected_entry(sel)
            text = view_entry(entry)
            self.type_text(text or '')
            return True

    def edit_entry(self, prompt=None):
        sel = self.dmenu_select(prompt, include_hidden=True)

        if sel:
            entry = self.get_selected_entry(sel)
            edit = True

            while edit is True:
                edit = edit_entry(self.kpo, entry)

            self.kpo.save()
            self.kpo = get_entries(self.database)
            return True

    def add_entry(self, **kwds):
        entry = add_entry(self.kpo)

        if entry:
            self.kpo.save()
            self.kpo = get_entries(self.database)
            self.prev_entry = entry
            return True

    def manage_groups(self, **kwds):
        group = manage_groups(self.kpo)

        if group:
            self.kpo.save()
            self.kpo = get_entries(self.database)
            return True

    def reload_db(self, **kwds):
        self.kpo = get_entries(self.database)

    def kill_daemon(self, **kwds):
        try:
            self.server.kill_flag.set()
        except (EOFError, IOError):
            pass

        return True

    def type_prev(self, **kwargs):
        if self.prev_text:
            self.type_text(self.prev_text or '')
            return True
        else:
            self.view_entry(**kwargs)

    def type_prev_username(self, **kwargs):
        if self.prev_entry:
            self.type_text(self.prev_entry.username or '')
            return True
        else:
            self.type_username(**kwargs)

    def show_prev_entry(self, **kwargs):
        if self.prev_entry:
            text = view_entry(self.prev_entry)
            self.type_text(text or '')
            return True
        else:
            self.view_entry(**kwargs)
        pass

    def type_prev_password(self, **kwargs):
        if self.prev_entry:
            self.type_text(self.prev_entry.password or '')
            return True
        else:
            self.type_password(**kwargs)

    def dmenu_select(self, prompt, *, include_hidden=False, options=None):
        kwds = {'prompt': prompt} if prompt else {}

        return view_all_entries(
            options or [],
            self.get_entries_descriptions(include_hidden=include_hidden),
            **kwds
        )

    def get_entries_descriptions(self, *, include_hidden=False):
        idx_align = len(str(len(self.kpo.entries)))

        return [
            _entry_description(idx, idx_align, entry)
            for idx, entry in enumerate(self.kpo.entries)
            if include_hidden or not self.is_hidden(entry)
        ]

    def get_selected_entry(self, description):
        if description:
            self.prev_entry = self.kpo.entries[_description_idx(description)]
            return self.prev_entry

        return None

    def is_hidden(self, entry):
        return any(group in entry.path for group in self.get_hidden_groups())

    def get_hidden_groups(self):
        # Validate ignored group names in config.ini

        if CONF.has_option("database", "hide_groups"):
            return [
                hg for hg in CONF.get("database", "hide_groups").split(",")
                if hg in [
                    g.name for g in self.kpo.groups
                ]
            ]

        return []


class Server(Process):
    """Run BaseManager server to listen for dmenu calling events

    """
    def __init__(self):
        Process.__init__(self)
        self.port, self.authkey = get_auth()
        self.start_q = Queue()
        self.kill_flag = Event()
        self.cache_time_expired = Event()

    def run(self):
        serv = self.server()  # pylint: disable=unused-variable
        self.kill_flag.wait()

    def server(self):
        """Set up BaseManager server

        """
        mgr = BaseManager(address=('127.0.0.1', self.port),
                          authkey=self.authkey)
        mgr.register('show_dmenu', callable=self.show_dmenu)
        mgr.start()
        return mgr

    def show_dmenu(self, args):
        if args.type_password is True:
            selected_option = MenuOption.TypePassword
        elif args.view_entry is True:
            selected_option = MenuOption.ViewEntry
        elif args.type_username is True:
            selected_option = MenuOption.TypeUsername
        elif args.type_entry is True:
            selected_option = MenuOption.TypeEntry
        elif args.type_prev_username is True:
            selected_option = MenuOption.TypePrevUsername
        elif args.type_prev_password is True:
            selected_option = MenuOption.TypePrevPassword
        elif args.show_prev_entry is True:
            selected_option = MenuOption.ShowPrevEntry
        elif args.type_prev is True:
            selected_option = MenuOption.TypePrev
        elif args.reloaddb is True:
            selected_option = MenuOption.ReloadDB
        else:
            selected_option = None

        self.start_q.put(selected_option)


def client():
    """Define client connection to server BaseManager

    Returns: BaseManager object
    """
    port, auth = get_auth()
    mgr = BaseManager(address=('', port), authkey=auth)
    mgr.register('show_dmenu')
    mgr.connect()
    return mgr


def start_server(args):
    """Main entrypoint. Start the background Manager and Dmenu runner processes.

    """
    server = Server()
    dmenu = DmenuRunner(server)
    dmenu.daemon = True
    server.start()
    dmenu.start()
    dbus = DbusRunner(server)
    dbus.start()
    server.show_dmenu(args)

    server.join()
    if exists(expanduser(AUTH_FILE)):
        os.remove(expanduser(AUTH_FILE))


def main():
    parser = argparse.ArgumentParser('keepmenu')
    parser.add_argument('--type-password', action='store_true', default='False', dest='type_password')
    parser.add_argument('--view-entry', action='store_true', default='False', dest='view_entry')
    parser.add_argument('--type-username', action='store_true', default='False', dest='type_username')
    parser.add_argument('--type-entry', action='store_true', default='False', dest='type_entry')
    parser.add_argument('--type-prev-username', action='store_true', default=False, dest='type_prev_username')
    parser.add_argument('--type-prev-password', action='store_true', default=False, dest='type_prev_password')
    parser.add_argument('--type-prev', action='store_true', default=False, dest='type_prev')
    parser.add_argument('--reloaddb', action='store_true', default=False, dest='reloaddb')
    parser.add_argument('--show-prev-entry', action='store_true', default=False, dest='show_prev_entry')

    args = parser.parse_args()

    try:
        LOG.info('Starting client')
        MANAGER = client()
        MANAGER.show_dmenu(args)  # pylint: disable=no-member
    except socket.error:
        LOG.info('Starting server')
        process_config()
        start_server(args)

if __name__ == '__main__':
    main()

# vim: set et ts=4 sw=4 :
